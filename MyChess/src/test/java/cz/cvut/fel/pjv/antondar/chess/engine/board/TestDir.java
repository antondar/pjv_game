package cz.cvut.fel.pjv.antondar.chess.engine.board;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class TestDir {
    @Test
    public void inverseTest() {
        // arrange
        Dir coords = new Dir(1, 5);
        Dir expectedResult = new Dir(1, -5);
        // act
        Dir result = coords.inverse();
        // assert
        Assertions.assertTrue(expectedResult.x == result.x && expectedResult.y == result.y);
    }

    @Test
    public void scaleTest() {
        // arrange
        int n = 2;
        Dir coords = new Dir(1, 3);
        Dir expectedResult = new Dir(2, 6);
        // act
        Dir result = coords.scale(n);
        // assert
        Assertions.assertTrue(expectedResult.x == result.x && expectedResult.y == result.y);
    }
}
