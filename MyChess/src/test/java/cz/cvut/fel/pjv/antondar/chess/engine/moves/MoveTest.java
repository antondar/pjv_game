package cz.cvut.fel.pjv.antondar.chess.engine.moves;
import cz.cvut.fel.pjv.antondar.chess.engine.board.*;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MoveTest {
    @Test
    public void PawnLongJumpMove_execute_Test() {
        Move.PawnLongJumpMove plm = new Move.PawnLongJumpMove(Pos.E7, Pos.E5);
        BoardMatrix board = createBoard();
        BoardMatrix newBoard = plm.execute(board);

        BoardMatrix expectedBoard = BoardMatrix.createEmpty();
        // White Layout
        expectedBoard.setAt(Pos.A1, Rook.White);
        expectedBoard.setAt(Pos.E1, King.White);
        // Black Layout
        expectedBoard.setAt(Pos.E5, Pawn.Black);
        expectedBoard.setAt(Pos.A8, Rook.Black);
        expectedBoard.setAt(Pos.E8, King.Black);
        expectedBoard.setAt(Pos.H8, Rook.Black);

        expectedBoard.setEnPassant(Pos.E5);

        Assertions.assertEquals(newBoard.toString(),expectedBoard.toString());
    }

    @Test
    public void PawnLongJumpMove_isLegal_Test() {
        Move.PawnLongJumpMove plm = new Move.PawnLongJumpMove(Pos.E7, Pos.E5);
        BoardMatrix board = createBoard();
        boolean res = plm.isLegal(board);
        Assertions.assertTrue(res);
    }


    @Test
    public void AttackMove_execute_Test() {
        Move.AttackMove am = new Move.AttackMove(Pos.A1, Pos.A8);
        BoardMatrix board = createBoard();
        BoardMatrix newBoard = am.execute(board);

        BoardMatrix expectedBoard = BoardMatrix.createEmpty();
        // White Layout
        expectedBoard.setAt(Pos.A8, Rook.White);
        expectedBoard.setAt(Pos.E1, King.White);
        // Black Layout
        expectedBoard.setAt(Pos.E7, Pawn.Black);
        expectedBoard.setAt(Pos.E8, King.Black);
        expectedBoard.setAt(Pos.H8, Rook.Black);

        expectedBoard.setEnPassant(Pos.OUT_OF_BOARD);

        Assertions.assertEquals(newBoard.toString(),expectedBoard.toString());
    }

    @Test
    public void AttackMove_isLegal_Test() {
        Move.AttackMove am = new Move.AttackMove(Pos.A1, Pos.A8);
        BoardMatrix board = createBoard();
        boolean res = am.isLegal(board);
        Assertions.assertTrue(res);
    }

    @Test
    public void LeftCastlingMove_execute_Test() {
        Move.LeftCastlingMove lcm = new Move.LeftCastlingMove(Pos.E1, Pos.A1);
        BoardMatrix board = createBoard();
        BoardMatrix newBoard = lcm.execute(board);

        BoardMatrix expectedBoard = BoardMatrix.createEmpty();
        // White Layout
        expectedBoard.setAt(Pos.D1, Rook.White);
        expectedBoard.setAt(Pos.C1, King.White);
        // Black Layout
        expectedBoard.setAt(Pos.E7, Pawn.Black);
        expectedBoard.setAt(Pos.A8, Rook.Black);
        expectedBoard.setAt(Pos.E8, King.Black);
        expectedBoard.setAt(Pos.H8, Rook.Black);

        expectedBoard.setEnPassant(Pos.OUT_OF_BOARD);

        Assertions.assertEquals(newBoard.toString(),expectedBoard.toString());
    }

    @Test
    public void LeftCastlingMove_isLegal_Test() {
        Move.LeftCastlingMove lcm = new Move.LeftCastlingMove(Pos.E1, Pos.A1);
        BoardMatrix board = createBoard();
        boolean res = lcm.isLegal(board);
        Assertions.assertTrue(res);
    }

    public BoardMatrix createBoard() {
        BoardMatrix board = new BoardMatrix();
        board = BoardMatrix.createEmpty();
        // White Layout
        board.setAt(Pos.A1, Rook.White);
        board.setAt(Pos.E1, King.White);

        // Black Layout
        board.setAt(Pos.E7, Pawn.Black);
        board.setAt(Pos.A8, Rook.Black);
        board.setAt(Pos.E8, King.Black);
        board.setAt(Pos.H8, Rook.Black);

        board.setEnPassant(Pos.OUT_OF_BOARD);
        return board;
    }
}
