package cz.cvut.fel.pjv.antondar.chess.engine.board;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class PosTest {
    static int value = 24;
    static Pos pos = new Pos(value);

    @Test
    public void getXTest() {
        int res = pos.getX();
        int expectedResult = 0;
        Assertions.assertEquals(res, expectedResult);
    }

    @ParameterizedTest(name = "{0} should be equal to {2}")
    @CsvSource({"1, 0", "24, 3"})
    public void getYTest(int a, int b) {
        // arrange
        Pos pos = new Pos(a);
        int expectedResult = b;
        // act
        int res = pos.getY();
        // assert
        Assertions.assertEquals(res, expectedResult);
    }
}