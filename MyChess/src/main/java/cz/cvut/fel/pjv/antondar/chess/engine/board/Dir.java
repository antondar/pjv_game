package cz.cvut.fel.pjv.antondar.chess.engine.board;

import lombok.Getter;

/**
 * Represents directions for move
 */
public class Dir {
    @Getter int x;
    @Getter int y;

    public Dir(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Inverse of move direction
     * @return recalculate direction
     */
    public Dir inverse() { return new Dir(x, -y); }

    /**
     * Scale move in certain direction
     * @param n ratio of scale
     * @return recalculate direction
     */
    public Dir scale(int n) { return new Dir(n*x, n*y); }

    // predefined constants
    public static final Dir UP = new Dir(0, 1);
    public static final Dir DOWN = new Dir(0, -1);
    public static final Dir LEFT = new Dir(-1, 0);
    public static final Dir RIGHT = new Dir(1, 0);

    public static final Dir UP_RIGHT = new Dir(1, 1);
    public static final Dir UP_LEFT = new Dir(-1, 1);
    public static final Dir DOWN_LEFT = new Dir(-1, -1);
    public static final Dir DOWN_RIGHT = new Dir(1, -1);

    public static final Dir L_UP_RIGHT = new Dir(1, 2);
    public static final Dir L_RIGHT_UP = new Dir(2, 1);
    public static final Dir L_RIGHT_DOWN = new Dir(2, -1);
    public static final Dir L_DOWN_RIGHT = new Dir(1, -2);
    public static final Dir L_DOWN_LEFT = new Dir(-1, -2);
    public static final Dir L_LEFT_DOWN = new Dir(-2, -1);
    public static final Dir L_LEFT_UP = new Dir(-2, 1);
    public static final Dir L_UP_LEFT = new Dir(-1, 2);

    public static final Dir UP_2 = new Dir(0, 2);
    public static final Dir KING_RIGHT_ROOK = new Dir(3, 0);
    public static final Dir KING_LEFT_ROOK = new Dir(-4, 0);
    public static final Dir KING_RIGHT_CASTLING = new Dir(2, 0);
    public static final Dir KING_LEFT_CASTLING = new Dir(-2, 0);
}
