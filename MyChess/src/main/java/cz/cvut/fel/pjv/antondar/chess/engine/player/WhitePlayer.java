package cz.cvut.fel.pjv.antondar.chess.engine.player;

import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.Move;

import java.util.Collection;

/**
 * Represent white player
 */
public class WhitePlayer extends Player {
    public WhitePlayer(BoardMatrix board, Collection<Move> legalMoves, boolean isCheck) {
        super(Alliance.WHITE, board, legalMoves, isCheck);
    }
}
