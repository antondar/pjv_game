package cz.cvut.fel.pjv.antondar.chess.engine.board;

import cz.cvut.fel.pjv.antondar.chess.engine.pieces.BoardPiece;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.Piece;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.PieceType;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Represents matrix collection of board pieces.
 */
public class BoardMatrix implements Cloneable, Comparable<BoardMatrix> {
    public static final int BOARD_SIZE = 8;

    private BoardPiece[][] pieces = createEmptyMatrix();

    /**
     * Position of current victim for EnPassant.
     */
    @Getter @Setter
    private Pos enPassant = Pos.OUT_OF_BOARD;

    /**
     * Returns piece at certain position
     * @param pos on board
     * @return piece or Empty if not found
     */
    public BoardPiece getAt(Pos pos) {
        Utils.checkOnBoard(pos);
        return pieces[pos.getY()][pos.getX()];
    }

    /**
     * Setting piece on board at certain position
     * @param pos to set
     * @param piece to set
     */
    public void setAt(Pos pos, Piece piece) {
        Utils.checkOnBoard(pos);
        pieces[pos.getY()][pos.getX()] = new BoardPiece(pos, piece);
    }

    /**
     * Removes piece from certain position
     * @param pos to remove
     */
    public void killAt(Pos pos) {
        Utils.checkOnBoard(pos);
        pieces[pos.getY()][pos.getX()] = BoardPiece.Empty;
    }

    /**
     * Collect all piece on the board, except Empty
     * @return collection of board pieces
     */
    public Collection<BoardPiece> getAllPieces() {
        return Arrays.stream(pieces)
                .flatMap(row -> Arrays.stream(row))
                .filter(boardPiece -> boardPiece != BoardPiece.Empty)
                .collect(Collectors.toUnmodifiableList());
    }

    /**
     * Collect all piece on the board, except Empty, for certain allience
     * @param alliance of player
     * @return collection of board pieces
     */
    public Collection<BoardPiece> getPieces(Alliance alliance) {
        return getAllPieces()
                .stream()
                .filter(piece -> piece.getPiece().getAlliance() == alliance)
                .collect(Collectors.toUnmodifiableList());
    }

    /**
     * Find king of certain alliance at the board
     * @param alliance of player
     * @return found king piece or Empty
     */
    public BoardPiece getKing(Alliance alliance) {
        Optional<BoardPiece> result = getPieces(alliance)
                .stream()
                .filter(piece -> piece.getPiece().getPieceType() == PieceType.KING)
                .findFirst();

        return result.isEmpty() ? BoardPiece.Empty : result.get();
    }

    /**
     * Reseets EnPassant position
     */
    public void resetEnPassant() { enPassant = Pos.OUT_OF_BOARD; }

    public static BoardMatrix createEmpty() {
        return new BoardMatrix();
    }

    /**
     * Deep cloning board.
     * @return new board.
     */
    @Override
    public BoardMatrix clone() {
        BoardMatrix newBoard = new BoardMatrix();
        getAllPieces().stream().forEach(boardPiece -> newBoard.setAt(boardPiece.getPosition(), boardPiece.getPiece()));
        newBoard.setEnPassant(getEnPassant());
        return newBoard;
    }

    @Override
    public String toString() {
        String boardPiecesStr = Arrays.stream(pieces)
                .map(row -> rowToString(row))
                .collect(Collectors.joining());

        String enPassantStr = String.format("np:%s", enPassant.toString());
        return boardPiecesStr + enPassantStr;

    }

    private String rowToString(BoardPiece[] row) {
        return Arrays.stream(row)
                .map(p -> pieceToString(p))
                .collect(Collectors.joining()) + '\n';
    }

    private String pieceToString(BoardPiece boardPiece) {
        if (boardPiece == BoardPiece.Empty)
            return ".";

        Piece piece = boardPiece.getPiece();
        String name = piece.getPieceType().toString();
        return piece.getAlliance().isWhite() ? name.toUpperCase() : name.toLowerCase();
    }

    @Override
    public int compareTo(BoardMatrix o) {
        if (o == null) return 1;

        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                BoardPiece a = this.pieces[i][j];
                BoardPiece b = o.pieces[i][j];

                if (a == b) continue;
                if (a == BoardPiece.Empty && b != BoardPiece.Empty) return -(i*BOARD_SIZE+j + 1);
                if (a != BoardPiece.Empty && b == BoardPiece.Empty) return i*BOARD_SIZE+j + 1;

                int pieceCmp = a.getPiece().getPieceType().compareTo(b.getPiece().getPieceType());
                if (pieceCmp != 0) return pieceCmp;
            }
        }

        return Integer.compare(this.enPassant.value, o.enPassant.value);
    }

    private static BoardPiece[][] createEmptyMatrix() {
        BoardPiece[][] pieces = new BoardPiece[BOARD_SIZE][BOARD_SIZE];
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                pieces[i][j] = BoardPiece.Empty;
            }
        }
        return pieces;
    }

    /**
     * Utility class.
     */
    public static class Utils {
        public static boolean isOnBoard(int ind) {
            return ind >= 0 && ind < BOARD_SIZE;
        }

        public static boolean isOnBoard(Pos pos) {
            return isOnBoard(pos.getX()) && isOnBoard(pos.getY());
        }

        public static void checkOnBoard(Pos pos) {
            if (!isOnBoard(pos))
                throw new IndexOutOfBoundsException(String.format("Illegal board coordinates: [x: %d, y: %d]", pos.getX(), pos.getY()));
        }
    }
}

