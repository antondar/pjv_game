package cz.cvut.fel.pjv.antondar.chess.engine.pieces;

import lombok.Getter;

/**
 * Names of pieces types
 */
public enum PieceType {
    PAWN("P"),
    KNIGHT("N"),
    ROOK("R"),
    BISHOP("B"),
    QUEEN("Q"),
    KING("K");

    @Getter
    private String pieceName;

    PieceType(final String pieceName) {
        this.pieceName = pieceName;
    }

    @Override
    public String toString() { return this.pieceName; }
}
