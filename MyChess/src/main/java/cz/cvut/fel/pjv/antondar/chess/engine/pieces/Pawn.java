package cz.cvut.fel.pjv.antondar.chess.engine.pieces;

import cz.cvut.fel.pjv.antondar.chess.engine.board.Dir;
import cz.cvut.fel.pjv.antondar.chess.engine.board.Pos;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.MovePattern;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;
/**
 * Represents class of Pawn
 */
public class Pawn extends Piece {
    private static final MovePattern[] regularMovePatterns = {
            // // Jump moves
            new MovePattern.JumpMovePattern(Dir.UP),
            new MovePattern.PawnLongJumpPattern(),

            // Attack moves
            new MovePattern.AttackMovePattern(Dir.UP_LEFT),
            new MovePattern.AttackMovePattern(Dir.UP_RIGHT),

            // En passant moves
            new MovePattern.EnPassantPawnMovePattern(Dir.UP_LEFT),
            new MovePattern.EnPassantPawnMovePattern(Dir.UP_RIGHT),
    };

    private static final MovePattern[] promoteMovePatterns = {
            new MovePattern.PromotePawnMovePattern()
    };

    private Pawn(Alliance alliance, Boolean hasMoved) {
        super(PieceType.PAWN, alliance, hasMoved);
    }

    @Override
    protected MovePattern[] getMovePatterns(Alliance alliance, Pos position) {
        // checks if a pawn advances to its eighth rank
        if ((alliance == Alliance.WHITE && position.is7Row())
            || (alliance == Alliance.BLACK && position.is2Row()))
            return promoteMovePatterns;

        return regularMovePatterns;
    }

    @Override
    public Piece move() { return alliance == Alliance.WHITE ? WhiteMoved : BlackMoved; }

    public static final Pawn White = new Pawn(Alliance.WHITE, false);
    public static final Pawn Black = new Pawn(Alliance.BLACK, false);
    public static final Pawn WhiteMoved = new Pawn(Alliance.WHITE, true);
    public static final Pawn BlackMoved = new Pawn(Alliance.BLACK, true);
}
