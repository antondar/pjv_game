package cz.cvut.fel.pjv.antondar.chess.engine.pieces;

import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.board.Pos;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.Move;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.MovePattern;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;

import lombok.Getter;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Represents abstract class of a piece
 */
public abstract class Piece {
    @Getter protected final PieceType pieceType;
    @Getter protected final Alliance alliance;
    // flag for recognize has the piece moved before
    @Getter protected final boolean moved;

    // Rules for moves
    protected abstract MovePattern[] getMovePatterns(Alliance alliance, Pos position);

    protected Piece(PieceType pieceType, Alliance alliance, boolean moved) {
        this.pieceType = pieceType;
        this.alliance = alliance;
        this.moved = moved;
    }

    public abstract Piece move();

    /**
     * Checks if alliance of other piece is the same
     * @param other is other piece
     * @return boolean
     */
    public boolean isOpposite(Piece other) {
        return alliance.opposite() == other.alliance;
    }

    /**
     * Collects all possible moves for a piece
     * @param board
     * @param position is tile coordinates
     * @return Collection of moves
     */
    public Collection<Move> populateMoves(BoardMatrix board, Pos position) {
        BoardPiece piece = board.getAt(position);
        return Arrays.stream(getMovePatterns(piece.getPiece().getAlliance(), position))
                .flatMap(pattern -> pattern.populateMoves(board, position).stream())
                .filter(move -> move != Move.EMPTY_MOVE)
                .collect(Collectors.toList());
    }
}

