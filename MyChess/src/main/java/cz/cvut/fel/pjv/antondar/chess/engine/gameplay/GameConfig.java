package cz.cvut.fel.pjv.antondar.chess.engine.gameplay;

import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;
import lombok.Getter;

import java.time.Duration;

/**
 * Represents setup config for game
 */
public class GameConfig {
    @Getter
    private final Alliance startPlayerAlliance;

    @Getter
    private final Game.Mode gameMode;

    @Getter
    private final Alliance aiPlayerAlliance;

    @Getter
    private final Duration timeLimit;

    @Getter
    private final BoardMatrix boardPattern;

    @Getter
    private final GameMeta gameMeta;

    /**
     * Creates game with chosen configuration
     * @param startPlayerAlliance player who starts first
     * @param gameMode PVP or PVE mode
     * @param aiPlayerAlliance alliance of AI player
     * @param timeLimit  limit time for game
     * @param boardPattern board with pieces
     * @param gameMeta metadata of the game
     */
    public GameConfig(Alliance startPlayerAlliance, Game.Mode gameMode, Alliance aiPlayerAlliance,
                      Duration timeLimit, BoardMatrix boardPattern, GameMeta gameMeta) {
        this.startPlayerAlliance = startPlayerAlliance;
        this.gameMode = gameMode;
        this.aiPlayerAlliance = aiPlayerAlliance;
        this.timeLimit = timeLimit;
        this.boardPattern = boardPattern;
        this.gameMeta = gameMeta;
    }
}
