package cz.cvut.fel.pjv.antondar.chess.engine.gameplay.thread;

import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.Game;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;

/**
 * Represents game thread for PvE (Player vs Environment)
 */
public class GameThreadPvERandomMove extends GameThread {
    /**
     * Creates game thread for PvE
     * @param game
     */
    public GameThreadPvERandomMove(Game game) {
        super(game);
    }

    @Override
    protected void iteration() {
        if (game.getState() != Game.GameState.Playing)
            return;

        // AI is waiting for his turn
        if (game.getAiPlayer() != game.getBoard().getCurrentPlayer().getAlliance())
            return;

        // AI execute move
        if (game.getAiPlayer().isWhite()) {
            game.randomMove(Alliance.WHITE);
        } else {
            game.randomMove(Alliance.BLACK);
        }
    }
}
