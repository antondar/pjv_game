package cz.cvut.fel.pjv.antondar.chess.engine.pieces;

import cz.cvut.fel.pjv.antondar.chess.engine.board.Dir;
import cz.cvut.fel.pjv.antondar.chess.engine.board.Pos;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.MovePattern;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;
/**
 * Represents class of Knight
 */
public class Knight extends Piece {
    // Rules for move
    private static final MovePattern[] movePatterns = {
            // Jump moves
            new MovePattern.JumpMovePattern(Dir.L_DOWN_LEFT),
            new MovePattern.JumpMovePattern(Dir.L_LEFT_DOWN),
            new MovePattern.JumpMovePattern(Dir.L_DOWN_RIGHT),
            new MovePattern.JumpMovePattern(Dir.L_RIGHT_DOWN),
            new MovePattern.JumpMovePattern(Dir.L_UP_LEFT),
            new MovePattern.JumpMovePattern(Dir.L_LEFT_UP),
            new MovePattern.JumpMovePattern(Dir.L_UP_RIGHT),
            new MovePattern.JumpMovePattern(Dir.L_RIGHT_UP),

            // Attack moves
            new MovePattern.AttackMovePattern(Dir.L_DOWN_LEFT),
            new MovePattern.AttackMovePattern(Dir.L_LEFT_DOWN),
            new MovePattern.AttackMovePattern(Dir.L_DOWN_RIGHT),
            new MovePattern.AttackMovePattern(Dir.L_RIGHT_DOWN),
            new MovePattern.AttackMovePattern(Dir.L_UP_LEFT),
            new MovePattern.AttackMovePattern(Dir.L_LEFT_UP),
            new MovePattern.AttackMovePattern(Dir.L_UP_RIGHT),
            new MovePattern.AttackMovePattern(Dir.L_RIGHT_UP),
    };

    private Knight(Alliance alliance, boolean hasMoved) {
        super(PieceType.KNIGHT, alliance, hasMoved);
    }

    @Override
    protected MovePattern[] getMovePatterns(Alliance alliance, Pos position) {
        return movePatterns;
    }

    @Override
    public Piece move() { return alliance == Alliance.WHITE ? WhiteMoved : BlackMoved; }

    public static final Knight White = new Knight(Alliance.WHITE, false);
    public static final Knight Black = new Knight(Alliance.BLACK, false);
    public static final Knight WhiteMoved = new Knight(Alliance.WHITE, true);
    public static final Knight BlackMoved = new Knight(Alliance.BLACK, true);
}
