package cz.cvut.fel.pjv.antondar.chess.engine.gameplay;

import cz.cvut.fel.pjv.antondar.chess.engine.board.Board;
import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.thread.GameThread;
import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.thread.GameThreadFactory;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.MoveCommand;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.MoveHistory;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;

import cz.cvut.fel.pjv.antondar.chess.engine.player.Player;
import cz.cvut.fel.pjv.antondar.chess.gui.controllers.BoardConfiguratorController;

import java.time.Duration;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * General class of game logic. Contains board, timers, move history, states and management logic.
 */
public class Game extends Observable {
    private Logger logger = Logger.getLogger(Game.class.getName());

    private Board board;

    private final MoveHistory moveHistory;


    private GameState state;

    private final Mode mode;

    private final MovePerformer movePerformer;

    private GameResult result;

    private final Alliance aiPlayer;

    private final Alliance startPlayer;

    private ChessClock whitePlayerTimer;

    private ChessClock blackPlayerTimer;

    private final GameThread thread;

    private long lastTimerCheck;

    /**
     * Initializes a game with certain parameters
     * @param board
     * @param mode PVP or PVE mode
     * @param startPlayer player who starts first
     * @param aiPlayer alliance of aiPlayer
     * @param timeLimit limit time for game
     * @param threadFactory game thread
     */
    public Game(Board board, Mode mode, Alliance startPlayer,
                Alliance aiPlayer, Duration timeLimit, GameThreadFactory threadFactory) {
        this.state = GameState.Initialization;
        this.result = GameResult.Unknown;
        this.board = board;
        this.moveHistory = new MoveHistory();
        this.mode = mode;
        this.startPlayer = startPlayer;
        this.aiPlayer = aiPlayer;
        this.movePerformer = new MovePerformer();
        this.thread = threadFactory.create(this);
        this.state = GameState.Created;
        this.whitePlayerTimer = new ChessClock(timeLimit);
        this.blackPlayerTimer = new ChessClock(timeLimit);
        this.lastTimerCheck = System.currentTimeMillis();
    }

    /**
     * Starts the game and game thread if game was created
     */
    public synchronized void start() {
        if (state != GameState.Created)
            throw new RuntimeException(String.format("Cannot start game now. Illegal state: %s", state));

        this.lastTimerCheck = System.currentTimeMillis();
        state = GameState.Playing;
        thread.start();
    }

    /**
     * Executes move, updates board
     * @param move execution move
     */
    public synchronized void move(MoveCommand move) {
        if (state != GameState.Playing)
            throw new RuntimeException(String.format("Cannot move now. Illegal state: %s", state));

        Board newBoard = movePerformer.perform(board, moveHistory, move);
        board = newBoard;
        logger.log(Level.INFO, "Sending notification on move complete");
//        System.out.println("Sending notification on move complete");
        actualizeState();
    }

    /**
     * Checks if is possible execute move
     * @param move execution move
     * @return boolean
     */
    public synchronized boolean canMove(MoveCommand move) {
        if (state != GameState.Playing)
            throw new RuntimeException(String.format("Cannot move now. Illegal state: %s", state));

        return movePerformer.canMove(board, move);
    }

    /**
     * Executes random move for ai, updates board
     * @param alliance
     */
    public synchronized void randomMove(Alliance alliance) { // for AI only
        if (state != GameState.Playing)
            throw new RuntimeException(String.format("Cannot move now. Illegal state: %s", state));

        Board newBoard = movePerformer.randomMove(board, moveHistory, alliance);
        board = newBoard;
        logger.log(Level.INFO, "Sending notification on move complete");
//        System.out.println("Sending notification on move complete");
        actualizeState();
    }

    /**
     * Terminates game with selected result
     * @param result chosen result
     */
    public synchronized void terminate(GameResult result) {
        try {
            state = GameState.Finished;
            this.result = result;
            thread.join(5000); // waiting for 5sec
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to waiting game thread finished, ignoring it");
        }
    }

    /**
     * Actualizes game states
     */
    public synchronized void actualizeState() {
        actualizeTimers();
        actualizeResult();
        setChanged();
        notifyObservers();
    }

    /**
     * Updates timer, decreases timer for active player
     */
    private void actualizeTimers() {
        if (state != GameState.Playing)
            return;

        long newTime = System.currentTimeMillis();
        Duration dur = Duration.ofMillis(newTime - lastTimerCheck);
        if (board.getCurrentPlayer().getAlliance() == Alliance.WHITE) {
            whitePlayerTimer.decrease(dur);
        } else {
            blackPlayerTimer.decrease(dur);
        }
        lastTimerCheck = newTime;
    }

    private void actualizeResult() {
        if (state != GameState.Playing)
            return;

        Player currentPlayer = board.getCurrentPlayer();

        if (currentPlayer.getAlliance().isWhite() && whitePlayerTimer.isOver()) {
            terminate(GameResult.BlackWin);
            return;
        }

        if (currentPlayer.getAlliance().isBlack() && blackPlayerTimer.isOver()) {
            terminate(GameResult.WhiteWin);
            return;
        }

        if (!currentPlayer.getLegalMoves().isEmpty())
            return;

        if (currentPlayer.isCheckMat()) {
            GameResult result = currentPlayer.getAlliance().isWhite() ? GameResult.BlackWin : GameResult.WhiteWin;
            terminate(result);
            return;
        }

        terminate(GameResult.Draw);
    }

    public synchronized Board getBoard() {
        return board;
    }

    public synchronized MoveHistory getMoveHistory() {
        return moveHistory;
    }

    public synchronized GameState getState() {
        return state;
    }

    public synchronized Mode getMode() {
        return mode;
    }

    public synchronized MovePerformer getMovePerformer() {
        return movePerformer;
    }

    public synchronized GameResult getResult() {
        return result;
    }

    public synchronized Alliance getAiPlayer() {
        return aiPlayer;
    }

    public synchronized Alliance getStartPlayer() {
        return startPlayer;
    }

    public synchronized ChessClock getWhitePlayerTimer() {
        return whitePlayerTimer;
    }

    public synchronized ChessClock getBlackPlayerTimer() {
        return blackPlayerTimer;
    }


    /**
     * Modes for play
     */
    public enum Mode { PVP, PVE }

    /**
     * Game states
     */
    public enum GameState { Initialization, Created, Playing, Finished }

    /**
     * Possible game results
     */
    public enum GameResult { WhiteWin, BlackWin, Draw, Unknown }
}