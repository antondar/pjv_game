package cz.cvut.fel.pjv.antondar.chess.engine.gameplay.thread;

import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.Game;

/**
 * Represent game threads factory for PvP, PvE
 */
public abstract class GameThreadFactory {
    /**
     * Abstract method for thread creation
     * @param game game
     * @return thread
     */
    public abstract GameThread create(Game game);

    /**
     * Thread factory for PvP
     */
    public static final GameThreadFactoryPvP PvP = new GameThreadFactoryPvP();

    /**
     * Thread factory for PvE
     */
    public static final GameThreadFactoryPvE PvE = new GameThreadFactoryPvE();

    private static final class GameThreadFactoryPvP extends GameThreadFactory {
        @Override
        public GameThread create(Game game) { return new GameThreadPvP(game); }
    }

    private static final class GameThreadFactoryPvE extends GameThreadFactory {
        @Override
        public GameThread create(Game game) { return new GameThreadPvERandomMove(game); }
    }
}
