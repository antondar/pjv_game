package cz.cvut.fel.pjv.antondar.chess.engine.player;

/**
 * Alliance in game
 */
public enum Alliance {
    WHITE, BLACK;

    public int getDirection() { return this == WHITE ? 1 : -1; }
    public boolean isWhite() { return this == WHITE; }
    public boolean isBlack() { return this == BLACK; }
    public Alliance opposite() { return this == WHITE ? BLACK : WHITE; }
}
