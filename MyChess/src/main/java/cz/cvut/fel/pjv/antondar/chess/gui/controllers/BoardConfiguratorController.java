package cz.cvut.fel.pjv.antondar.chess.gui.controllers;

import cz.cvut.fel.pjv.antondar.*;
import cz.cvut.fel.pjv.antondar.chess.gui.ViewModelManager;
import cz.cvut.fel.pjv.antondar.chess.gui.controls.board.ChessPane;
import cz.cvut.fel.pjv.antondar.chess.gui.controls.stash.PieceStashPane;
import cz.cvut.fel.pjv.antondar.chess.gui.models.BoardConfiguratorViewModel;
import cz.cvut.fel.pjv.antondar.chess.gui.models.GameSetupViewModel;
import cz.cvut.fel.pjv.antondar.chess.gui.models.GameViewModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents board configurator controller, managing FXML board view
 */
public class BoardConfiguratorController {
    private Logger logger = Logger.getLogger(BoardConfiguratorController.class.getName());
    private final GameSetupViewModel gameSetupModel = ViewModelManager.getInstance().getGameSetup();
    private final BoardConfiguratorViewModel boardConfModel = ViewModelManager.getInstance().getGameSetup().getBoardConfiguratorViewModel();
    private final GameViewModel gameModel = ViewModelManager.getInstance().getGame();

    @FXML
    ChessPane chessPane;

    @FXML
    PieceStashPane stashPane;

    @FXML
    Button startButton;

    /**
     * Post-creation initialization.
     * Sets processing strategies for movement actions of board and stash controls.
     * Binds view-model board with control board, binds stash with update signals.
     */
    @FXML
    public void initialize() {
        chessPane.setTileStrategyContainer(boardConfModel.getBoardStrategyContainer());
        stashPane.setTileStrategyContainer(boardConfModel.getStashStrategyContainer());
        chessPane.getBoardProperty().bind(boardConfModel.getBoardPattern());
        stashPane.getChangeProperty().bind(boardConfModel.getBoardPattern());
    }

    /**
     * Handler for start game button click
     */
    @FXML
    private void startGame(ActionEvent ae) throws IOException {
        // king should be on board
        if (!boardConfModel.checkBoardValid()) {
            logger.log(Level.INFO, ()->String.format("Illegal board %s", boardConfModel.getBoardPattern().get()));
//            System.out.println(String.format("Illegal board %s", boardConfModel.getBoardPattern().get()));
            App.showModalAlert("Illegal board configuration, fix it!");
            return;
        }

        gameSetupModel.saveGameConfig();
        gameModel.recreateGame();
        App.setRoot("Game");
    }
}
