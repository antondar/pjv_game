package cz.cvut.fel.pjv.antondar.chess.engine.board;

import cz.cvut.fel.pjv.antondar.chess.engine.moves.Move;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.MoveCommand;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;
import cz.cvut.fel.pjv.antondar.chess.engine.player.BlackPlayer;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Player;
import cz.cvut.fel.pjv.antondar.chess.engine.player.WhitePlayer;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.*;
import lombok.Getter;

import java.util.*;

/**
 * Represents game board, players with their possible moves and player who is turn
 */
public class Board {
    @Getter
    private final BoardMatrix board;

    @Getter
    private final WhitePlayer whitePlayer;

    @Getter
    private final BlackPlayer blackPlayer;

    @Getter
    private final Player currentPlayer;

    public Board(final BoardMatrix boardMatrix,
                 final WhitePlayer whitePlayer,
                 final BlackPlayer blackPlayer,
                 final Alliance currentPlayer) {
        this.board = boardMatrix;
        this.whitePlayer = whitePlayer;
        this.blackPlayer = blackPlayer;
        this.currentPlayer = currentPlayer.isWhite() ? whitePlayer : blackPlayer;
    }

    /**
     * @param command contains the source and destination positions
     * @return Move.EMPTY_MOVE if source position is empty or
     * if destination positions is unreachable by a piece. Otherwise return Move of a piece.
     */
    public Move findMove(MoveCommand command) {
        BoardPiece boardPiece = board.getAt(command.getFrom());
        if (boardPiece == BoardPiece.Empty)
            return Move.EMPTY_MOVE;

        Piece piece = boardPiece.getPiece();

        Optional<Move> moveResult = getMoves(piece.getAlliance())
                .stream()
                .filter(move -> move.getSrc() == command.getFrom() && move.getDest() == command.getTo())
                .findFirst();

        return moveResult.isEmpty() ? Move.EMPTY_MOVE : moveResult.get();
    }

    /**
     * @param alliance
     * @return Legal moves' collection of player according to his alliance.
     */
    public Collection<Move> getMoves(Alliance alliance) {
        final Player player = alliance.isWhite() ? whitePlayer : blackPlayer;
        return player.getLegalMoves();
    }

    /**
     * Places pieces on a board
     * @return standard initial board representation
     */
    public static BoardMatrix createStandardBoardMatrix() {
        BoardMatrix board = BoardMatrix.createEmpty();
        // White Layout
        board.setAt(Pos.A1, Rook.White);
        board.setAt(Pos.B1, Knight.White);
        board.setAt(Pos.C1, Bishop.White);
        board.setAt(Pos.D1, Queen.White);
        board.setAt(Pos.E1, King.White);
        board.setAt(Pos.F1, Bishop.White);
        board.setAt(Pos.G1, Knight.White);
        board.setAt(Pos.H1, Rook.White);

        board.setAt(Pos.A2, Pawn.White);
        board.setAt(Pos.B2, Pawn.White);
        board.setAt(Pos.C2, Pawn.White);
        board.setAt(Pos.D2, Pawn.White);
        board.setAt(Pos.E2, Pawn.White);
        board.setAt(Pos.F2, Pawn.White);
        board.setAt(Pos.G2, Pawn.White);
        board.setAt(Pos.H2, Pawn.White);

        // Black Layout
        board.setAt(Pos.A7, Pawn.Black);
        board.setAt(Pos.B7, Pawn.Black);
        board.setAt(Pos.C7, Pawn.Black);
        board.setAt(Pos.D7, Pawn.Black);
        board.setAt(Pos.E7, Pawn.Black);
        board.setAt(Pos.F7, Pawn.Black);
        board.setAt(Pos.G7, Pawn.Black);
        board.setAt(Pos.H7, Pawn.Black);

        board.setAt(Pos.A8, Rook.Black);
        board.setAt(Pos.B8, Knight.Black);
        board.setAt(Pos.C8, Bishop.Black);
        board.setAt(Pos.D8, Queen.Black);
        board.setAt(Pos.E8, King.Black);
        board.setAt(Pos.F8, Bishop.Black);
        board.setAt(Pos.G8, Knight.Black);
        board.setAt(Pos.H8, Rook.Black);

        board.setEnPassant(Pos.OUT_OF_BOARD);

        return board;
    }
}
