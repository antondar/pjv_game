package cz.cvut.fel.pjv.antondar.chess.engine.moves;

import cz.cvut.fel.pjv.antondar.chess.engine.board.Board;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents snapshot of game state
 */
class MoveSnapshot {
    @Getter
    private final Board board;

    @Getter
    private final Move move;

    public MoveSnapshot(Board board, Move move) {
        this.board = board;
        this.move = move;
    }
}

/**
 * Represents game history of snapshots
 */
public class MoveHistory {
    private List<MoveSnapshot> snapshots = new ArrayList<MoveSnapshot>();

    public void add(Board board, Move move) {
        snapshots.add(new MoveSnapshot(board, move));
    }

    /**
     * @return all performed moves
     */
    public Collection<Move> getMoves() {
        return snapshots
                .stream()
                .map(s -> s.getMove())
                .collect(Collectors.toList());
    }
}
