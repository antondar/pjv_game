package cz.cvut.fel.pjv.antondar.chess.engine.gameplay;

import java.time.Duration;

/**
 * Represents timer for game
 */
public class ChessClock {
    public static final Duration DEFAULT = Duration.ofMinutes(15);

    private Duration time;

    public ChessClock(Duration time) {
        if (time.isNegative())
            throw new IllegalArgumentException("Value of limit cannot be negative");

        this.time = time;
    }

    /**
     * Updates timer, decreasing by value
     * @param value
     */
    public synchronized void decrease(Duration value) {
        if (value.isNegative())
            throw new IllegalArgumentException("Decreasing by negative value is denied");

        time = max(time.minus(value), Duration.ZERO);
    }

    /**
     * check if is time over
     * @return check result
     */
    public synchronized boolean isOver() {
        return time.compareTo(Duration.ZERO) == 0;
    }

    /**
     * @return pretty string view of time
     */
    public synchronized String toClockString() {
        long s = time.toSeconds();
        return String.format("%02d:%02d:%02d", s / 3600, (s % 3600) / 60, (s % 60));
    }

    public synchronized Duration getTime() {
        return time;
    }

    private Duration max(Duration first, Duration second) {
        return first.compareTo(second) >= 0 ? first : second;
    }
}
