package cz.cvut.fel.pjv.antondar.chess.engine.serialization;

import cz.cvut.fel.pjv.antondar.chess.engine.board.Pos;
import lombok.Getter;
import lombok.Setter;

/**
 * Defines class for piece position serializable type, acceptable by serializer.
 */
public class PositionSerializableData {
    @Getter @Setter
    private final int x;

    @Getter @Setter
    private final int y;

    public PositionSerializableData(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Constructs from engine logic piece position class
     */
    public static PositionSerializableData fromPos(Pos pos) {
        return new PositionSerializableData(pos.getX(), pos.getY());
    }
}
