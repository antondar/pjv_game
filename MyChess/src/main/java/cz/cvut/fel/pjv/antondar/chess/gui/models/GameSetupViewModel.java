package cz.cvut.fel.pjv.antondar.chess.gui.models;

import cz.cvut.fel.pjv.antondar.chess.engine.board.Board;
import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.Game;
import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.GameConfig;
import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.Gameplay;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;

import cz.cvut.fel.pjv.antondar.chess.gui.controls.board.ChessPane;
import javafx.beans.property.SimpleStringProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.Duration;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * View-model for game setup window.
 */
public class GameSetupViewModel {
    private Logger logger = Logger.getLogger(GameSetupViewModel.class.getName());
    // Predefined constant standard board
    private static BoardMatrix DEFAULT_BOARD = Board.createStandardBoardMatrix();

    private final Gameplay gameplay;

    /**
     * Defines property for binding with start player alliance radio button.
     */
    @Getter @Setter
    private Alliance startPlayerAlliance = Alliance.WHITE;

    /**
     * Defines property for binding with game mode radio button.
     */
    @Getter @Setter
    private Game.Mode gameMode = Game.Mode.PVP;

    /**
     * Defines property for binding with ai player alliance radio button.
     */
    @Getter @Setter
    private Alliance aiPlayerAlliance = Alliance.BLACK;

    /**
     * Defines property for binding with custom board checkbox.
     */
    @Getter @Setter
    private boolean isCustomBoard = false;

    /**
     * Defines property for binding with time limit field.
     */
    @Getter @Setter
    private SimpleStringProperty timeLimitMinutes = new SimpleStringProperty("15");

    /**
     * Sub class of game-meta view-model for binding with game-meta fields.
     */
    @Getter
    private GameMetaViewModel gameMetaViewModel = new GameMetaViewModel();

    /**
     * Sub class of custom board configuration view-model for binding with custom board pattern.
     */
    @Getter
    private BoardConfiguratorViewModel boardConfiguratorViewModel = new BoardConfiguratorViewModel();

    /**
     * Creates game setups
     * @param gameplay
     */
    public GameSetupViewModel(Gameplay gameplay) {
        this.gameplay = gameplay;
    }

    /**
     * Creates game configurations and sets them to gameplay
     */
    public void saveGameConfig() {
        // limit
        int limitMinutes = Integer.parseInt(timeLimitMinutes.getValue());
        // set board type
        BoardMatrix boardPattern = isCustomBoard ? boardConfiguratorViewModel.getBoardPattern().get() : DEFAULT_BOARD;

        logger.log(Level.INFO, ()->String.format("Saving config. Is custom board: %s, board: \n%s", isCustomBoard, boardPattern));
//        System.out.println(String.format("Saving config. Is custom board: %s, board: \n%s", isCustomBoard, boardPattern));

        GameConfig config = new GameConfig(
                startPlayerAlliance,
                gameMode,
                aiPlayerAlliance,
                Duration.ofMinutes(limitMinutes),
                boardPattern.clone(),
                gameMetaViewModel.createMeta()
        );

        gameplay.setGameConfig(config);
    }
}
