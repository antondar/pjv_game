package cz.cvut.fel.pjv.antondar.chess.gui.models;

import cz.cvut.fel.pjv.antondar.chess.engine.serialization.PgnUtils;
import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.Game;
import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.Gameplay;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.MoveCommand;
import cz.cvut.fel.pjv.antondar.chess.gui.controls.StrategyContainer;
import cz.cvut.fel.pjv.antondar.chess.gui.controls.TileStrategy;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import lombok.Getter;

import java.io.File;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * View-model for game window.
 */
public class GameViewModel implements Observer {
    private Logger logger = Logger.getLogger(GameViewModel.class.getName());
    // certain game settings
    private final Gameplay gameplay;
    private Game game;

    /**
     * Defines property for binding with game state label.
     */
    @Getter
    private final SimpleStringProperty state = new SimpleStringProperty("state");

    /**
     * Defines property for binding with game result label.
     */
    @Getter
    private final SimpleStringProperty result = new SimpleStringProperty("result");

    /**
     * Defines property for binding with white player timer label.
     */
    @Getter
    private final SimpleStringProperty whitePlayerTimer = new SimpleStringProperty("Timer 00:00:00");

    /**
     * Defines property for binding with black player timer label.
     */
    @Getter
    private final SimpleStringProperty blackPlayerTimer = new SimpleStringProperty("Timer 00:00:00");

    /**
     * Defines property for binding with chess panel board matrix.
     */
    @Getter
    private final SimpleObjectProperty<BoardMatrix> board = new SimpleObjectProperty<>(BoardMatrix.createEmpty());

    /**
     * Defines strategy for chess panel control.
     */
    @Getter
    private StrategyContainer strategyContainer = new StrategyContainer(new TileStrategy.NullTileStrategy());

    /**
     * Creates game view model
     * @param gameplay object for operation with game
     */
    public GameViewModel(Gameplay gameplay) {
        this.gameplay = gameplay;
        this.game = gameplay.getGame();
    }

    /**
     * Creates new game.
     */
    public void recreateGame() {
        if (game != null)
            terminateGame();
        gameplay.createGame();
        game = gameplay.getGame();
        strategyContainer.setStrategy(new TileStrategy.DeniedTileStrategy());
        init();
    }

    /**
     * Terminate game: setup game result to unknown,
     * unsubscribing from game events
     */
    public void terminateGame() {
        if (game == null)
            return;

        gameplay.terminateGame();
        completeGame();
    }

    // complete by unsubscribing from game events
    private void completeGame() {
        strategyContainer.setStrategy(new TileStrategy.DeniedTileStrategy());
        unsubscribeFromGame();
        game = null;
    }

    /**
     * Initializes game:
     * Subscribes to game (add observer) and
     * update timer, board, result
     */
    public void init() {
        subscribeToGame();
        updateGameCounters();
    }

    /**
     * Start game: start thread, change gameplay state
     */
    public void start() {
        gameplay.startGame();
        strategyContainer.setStrategy(new TileStrategy.GameTileStrategy(game));
    }

    /**
     * Restart game
     */
    public void restart() {
        recreateGame();
        start();
    }

    /**
     * Resign game
     */
    public void resign() {
        gameplay.resignGame();
        completeGame();
    }

    /**
     * Saves game to external file
     * @param file
     */
    public void saveGame(File file) throws Exception {
        if (game == null)
            throw new Exception("Game is not created yet");

        PgnUtils.write(file, gameplay.getGameConfig(), game);
    }

    /**
     * UpdateS timer, board, result
     * @param o  an observer to be added
     * @param arg
     */
    @Override
    public void update(Observable o, Object arg) {
        updateGameCounters();
    }

    private void updateGameCounters() {
        updateTimers();
        updateGameStateResult();
        updateBoardIfRequired();
    }

    private void updateTimers() {
        // for execution in GUI form context, otherwise exception occurred
        Platform.runLater(() -> {
            String whiteTimerStr = game.getWhitePlayerTimer().toClockString();
            whitePlayerTimer.set(whiteTimerStr);

            String blackTimerStr = game.getBlackPlayerTimer().toClockString();
            blackPlayerTimer.set(blackTimerStr);
        });
    }

    private void updateBoardIfRequired() {
        // for execution in GUI form context, otherwise exception occurred
        Platform.runLater(() -> {
            BoardMatrix boardMatrix = game.getBoard().getBoard();

            BoardMatrix curBoard = board.get();

            if (isUpdateRequired(curBoard, boardMatrix)) {
                logger.log(Level.INFO, ()->String.format("Board should be updated old board:\n%s, new board:\n%s", curBoard, boardMatrix));
//                System.out.println(String.format("Board should be updated old board:\n%s, new board:\n%s", curBoard, boardMatrix));
                board.set(boardMatrix);
            }
        });
    }

    private void updateGameStateResult() {
        // for execution in GUI form context, otherwise exception occurred
        Platform.runLater(() -> {
            String gameStateStr = game.getState().toString();
            state.set(gameStateStr);

            String gameResultStr = game.getResult().toString();
            result.set(gameResultStr);
        });
    }

    private boolean isUpdateRequired(BoardMatrix oldBoard, BoardMatrix newBoard) {
        return oldBoard.compareTo(newBoard) != 0;
    }

    private void subscribeToGame() {
        game.addObserver(this);
    }

    private void unsubscribeFromGame() {
        game.deleteObservers();
    }
}
