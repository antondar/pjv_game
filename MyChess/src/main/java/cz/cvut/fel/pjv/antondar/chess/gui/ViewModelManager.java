package cz.cvut.fel.pjv.antondar.chess.gui;

import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.Gameplay;
import cz.cvut.fel.pjv.antondar.chess.gui.models.GameSetupViewModel;
import cz.cvut.fel.pjv.antondar.chess.gui.models.GameViewModel;
import lombok.Getter;

/**
 * Singleton, represents main point of GUI view-models operating gameplay.
 * View-models are used as a facade for game logic with goal to split engine core
 * and visualization. It also holds properties for (bi)directional bindings of controls' models.
 */
public final class ViewModelManager {
    private final Gameplay gameplay = new Gameplay();

    @Getter
    private GameViewModel Game = new GameViewModel(gameplay);

    @Getter
    private GameSetupViewModel GameSetup = new GameSetupViewModel(gameplay);

    private ViewModelManager() {}

    @Getter
    private static ViewModelManager instance = new ViewModelManager();
}
