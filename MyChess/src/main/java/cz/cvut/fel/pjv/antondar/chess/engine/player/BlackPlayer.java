package cz.cvut.fel.pjv.antondar.chess.engine.player;

import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.Move;

import java.util.Collection;
/**
 * Represent black player
 */
public class BlackPlayer extends Player {
    public BlackPlayer(BoardMatrix board, Collection<Move> legalMoves, boolean isCheck) {
        super(Alliance.BLACK, board, legalMoves, isCheck);
    }
}
