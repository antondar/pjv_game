package cz.cvut.fel.pjv.antondar.chess.gui.controls.stash;

import cz.cvut.fel.pjv.antondar.chess.gui.controls.ImageUtils;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.*;
import cz.cvut.fel.pjv.antondar.chess.gui.controls.PieceContainer;
import cz.cvut.fel.pjv.antondar.chess.gui.controls.StrategyContainer;
import cz.cvut.fel.pjv.antondar.chess.gui.controls.TileStrategy;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import lombok.Getter;

/**
 * Control represents stash pane of pieces under the board, that can be source for population board pieces.
 * Extents classic javafx panel.
 */
public class PieceStashPane extends Pane {
    public StrategyContainer tileStrategyContainer = new StrategyContainer(new TileStrategy.NullTileStrategy());

    /**
     * Property for updates subscribing.
     */
    @Getter
    private final SimpleObjectProperty<Object> changeProperty = new SimpleObjectProperty<Object>(new Object());

    public PieceStashPane() {
        super();
        changeProperty.addListener(this::onStashChanged);
        draw();
    }

    private void onStashChanged(ObservableValue<? extends Object> prop,
                               Object oldValue,
                               Object newValue) {
        redraw();
    }

    /**
     * Setting container with move strategy.
     * @param container of strategy.
     */
    public void setTileStrategyContainer(StrategyContainer container) {
        tileStrategyContainer = container;
        redraw();
    }

    private void redraw() {
        getChildren().clear();
        draw();
    }

    /**
     * Draw control.
     */
    public void draw() {
        double collSize = 40;

        for (int i = 0; i < pieces.length; i++) {
            Piece piece = pieces[i];
            PieceContainer sp = new PieceContainer(i, collSize, true, tileStrategyContainer);

            getChildren().add(sp);

            sp.setLayoutX(i * collSize);
            sp.setLayoutY(0);

            Image image = ImageUtils.LoadImage(piece, this.getClass().getClassLoader());
            ImageView iv = sp.getImageView();
            iv.setImage(image);
        }
    }

    private static final Piece[] pieces = {
            Rook.White,
            Knight.White,
            Bishop.White,
            Queen.White,
            King.White,
            Pawn.White,

            Rook.Black,
            Knight.Black,
            Bishop.Black,
            Queen.Black,
            King.Black,
            Pawn.Black
    };

    /**
     * Returns pieces corresponding to tile index.
     * @param index index of container in stash
     * @return piece
     */
    public static Piece getPiece(int index) {
        if (index < 0 || index >= pieces.length)
            throw new RuntimeException(String.format("Invalid piece index: %d", index));

        return pieces[index];
    }
}
