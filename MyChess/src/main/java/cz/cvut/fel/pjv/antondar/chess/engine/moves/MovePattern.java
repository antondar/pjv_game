package cz.cvut.fel.pjv.antondar.chess.engine.moves;

import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.board.Dir;
import cz.cvut.fel.pjv.antondar.chess.engine.board.Pos;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.BoardPiece;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.Piece;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.PieceType;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Defines patterns for moves population. Populates all move (without legality check).
 */
public abstract class MovePattern {
    /**
     * Populate moves by pattern
     * @param board used for calculations
     * @param src position move started at
     * @return collection of generated moves
     */
    public abstract Collection<Move> populateMoves(BoardMatrix board, Pos src);

    /**
     * Move pattern for jump to certain tile.
     */
    public static final class JumpMovePattern extends MovePattern {
        private final Dir direction;

        public JumpMovePattern(Dir direction) {
            this.direction = direction;
        }

        @Override
        public Collection<Move> populateMoves(BoardMatrix board, Pos src) {
            BoardPiece boardPiece = board.getAt(src);
            if (boardPiece == BoardPiece.Empty)
                throw new RuntimeException(String.format("Illegal move source, piece not exists at: %s", src.toString()));

            ArrayList<Move> moves = new ArrayList<>();
            Pos dest = boardPiece.calcDestination(direction);
            if (canMoveTo(board, dest))
                moves.add(new Move.JumpMove(src, dest));

            return moves;
        }

        public static boolean canMoveTo(BoardMatrix board, Pos dest) {
            return dest != Pos.OUT_OF_BOARD // can't move out of board
                    && board.getAt(dest) == BoardPiece.Empty; // can't move to occupied tile
        }
    }

    /**
     * Move pattern for scalable jump to certain tile.
     * Populates moves for all reachable tiles in selected direction until first occupied tile resolved.
     */
    public static final class ScaleJumpMovePattern extends MovePattern {
        private final Dir direction;
        private final int ratio;

        public ScaleJumpMovePattern(Dir direction, int ratio) {
            this.direction = direction;
            this.ratio = ratio;
        }

        @Override
        public Collection<Move> populateMoves(BoardMatrix board, Pos src) {
            BoardPiece boardPiece = board.getAt(src);
            if (boardPiece == BoardPiece.Empty)
                throw new RuntimeException(String.format("Illegal move source, piece not exists at: %s", src.toString()));

            ArrayList<Move> moves = new ArrayList<>();

            for (int i = 1; i <= ratio; i++) {
                Pos dest = boardPiece.calcDestination(direction.scale(i));

                if (JumpMovePattern.canMoveTo(board, dest))
                    moves.add(new Move.JumpMove(src, dest)); // valid for jump
                else
                    break; // can't continue moving since first illegal tile reached
            }

            return moves;
        }
    }

    /**
     * Move pattern for attack on certain tile.
     */
    public static final class AttackMovePattern extends MovePattern {
        private final Dir direction;

        public AttackMovePattern(Dir direction) {
            this.direction = direction;
        }

        @Override
        public Collection<Move> populateMoves(BoardMatrix board, Pos src) {
            BoardPiece boardPiece = board.getAt(src);
            if (boardPiece == BoardPiece.Empty)
                throw new RuntimeException(String.format("Illegal move source, piece not exists at: %s", src.toString()));

            ArrayList<Move> moves = new ArrayList<>();
            Pos dest = boardPiece.calcDestination(direction);
            if (canAttackAt(board, boardPiece, dest))
                moves.add(new Move.AttackMove(src, dest));

            return moves;
        }

        public static boolean canAttackAt(BoardMatrix board, BoardPiece piece, Pos dest) {
            BoardPiece attacked;
            return dest != Pos.OUT_OF_BOARD // can't attack out of board
                    && (attacked = board.getAt(dest)) != BoardPiece.Empty // can't attack empty tile
                    && piece.getPiece().isOpposite(attacked.getPiece()); // alliance check
        }
    }

    /**
     * Move pattern for scalable attack on certain tile.
     * Populates move for first victim piece in selected direction.
     */
    public static final class ScaleAttackMovePattern extends MovePattern {
        private final Dir direction;
        private final int ratio;

        public ScaleAttackMovePattern(Dir direction, int ratio) {
            this.direction = direction;
            this.ratio = ratio;
        }

        public Collection<Move> populateMoves(BoardMatrix board, Pos src) {
            BoardPiece boardPiece = board.getAt(src);
            if (boardPiece == BoardPiece.Empty)
                throw new RuntimeException(String.format("Illegal move source, piece not exists at: %s", src.toString()));

            ArrayList<Move> moves = new ArrayList<>();

            for (int i = 1; i <= ratio; i++) {
                Pos dest = boardPiece.calcDestination(direction.scale(i));
                if (dest == Pos.OUT_OF_BOARD)
                    break; // can't continue attack out of board

                BoardPiece attacked = board.getAt(dest);
                if (attacked == BoardPiece.Empty)
                    continue; // skipping tile

                boolean isOpposite = boardPiece.getPiece().isOpposite(attacked.getPiece());
                if (isOpposite)
                    moves.add(new Move.AttackMove(src, dest)); // valid for attack

                break; // can't continue attack since first occupied tile reached
            }

            return moves;
        }
    }

    /**
     * Move pattern for En Passant pawn move.
     */
    public static final class EnPassantPawnMovePattern extends MovePattern {
        private final Dir direction;

        public EnPassantPawnMovePattern(Dir direction) {
            this.direction = direction;
        }

        @Override
        public Collection<Move> populateMoves(BoardMatrix board, Pos src) {
            BoardPiece boardPiece = board.getAt(src);
            if (boardPiece == BoardPiece.Empty)
                throw new RuntimeException(String.format("Illegal move source, piece not exists at: %s", src.toString()));

            ArrayList<Move> moves = new ArrayList<>();
            Pos dest = boardPiece.calcDestination(direction);
            if (canAttackAt(board, boardPiece, dest))
                moves.add(new Move.EnPassantMove(src, dest));

            return moves;
        }

        private static boolean canAttackAt(BoardMatrix board, BoardPiece piece, Pos dest) {
            final Pos enPassantPawnPos = board.getEnPassant();
            if (enPassantPawnPos.equals(Pos.OUT_OF_BOARD))
                return false;

            final BoardPiece enPassantPiece = board.getAt(enPassantPawnPos);
            if (enPassantPiece == BoardPiece.Empty)
                throw new RuntimeException(String.format("Illegal state, enpassant pawn not found at: %s", enPassantPawnPos));

            PieceType enpassantPieceType = enPassantPiece.getPiece().getPieceType();
            if (enpassantPieceType != PieceType.PAWN)
                throw new RuntimeException(String.format("Illegal state, piece at enpassant tile is not a pawn: %s (%s)",
                        enPassantPawnPos, enpassantPieceType.name()));

            Pos attackedPos = enPassantPiece.calcDestination(Dir.DOWN);
            return attackedPos != Pos.OUT_OF_BOARD // can't attack out of board
                    && dest.equals(attackedPos) // can attack only previous moved en passant victim pawn at jumped place
                    && board.getAt(dest) == BoardPiece.Empty // place for en passant should be empty
                    && piece.getPiece().isOpposite(enPassantPiece.getPiece()); // alliance check
        }
    }

    /**
     * Move pattern for first pawn long jump.
     */
    public static final class PawnLongJumpPattern extends MovePattern {
        @Override
        public Collection<Move> populateMoves(BoardMatrix board, Pos src) {
            BoardPiece boardPiece = board.getAt(src);
            if (boardPiece == BoardPiece.Empty)
                throw new RuntimeException(String.format("Illegal move source, piece not exists at: %s", src.toString()));

            Piece piece = boardPiece.getPiece();
            if (piece.getPieceType() != PieceType.PAWN)
                throw new RuntimeException(String.format("Illegal move source, piece is not a Pawn at: %s", src.toString()));

            ArrayList<Move> moves = new ArrayList<>();
            Pos dest1 = boardPiece.calcDestination(Dir.UP);
            Pos dest2 = boardPiece.calcDestination(Dir.UP_2);
            if (JumpMovePattern.canMoveTo(board, dest1)
                    && JumpMovePattern.canMoveTo(board, dest2)
                    && !piece.isMoved())
                moves.add(new Move.PawnLongJumpMove(src, dest2));

            return moves;
        }
    }

    /**
     * Move pattern for promoting pawn at the end of board.
     */
    public static final class PromotePawnMovePattern extends MovePattern {
        @Override
        public Collection<Move> populateMoves(BoardMatrix board, Pos src) {
            BoardPiece boardPiece = board.getAt(src);
            if (boardPiece == BoardPiece.Empty)
                throw new RuntimeException(String.format("Illegal move source, piece not exists at: %s", src.toString()));

            Piece promotePiece = boardPiece.getPiece();
            if (promotePiece.getPieceType() != PieceType.PAWN)
                throw new RuntimeException(String.format("Illegal move source, piece is not a Pawn at: %s", src.toString()));

            ArrayList<Move> moves = new ArrayList<>();
            Pos dest = boardPiece.calcDestination(Dir.UP);
            if (JumpMovePattern.canMoveTo(board, dest))
                moves.add(new Move.PromoteMove(src, dest));

            Pos lAttackDest = boardPiece.calcDestination(Dir.UP_LEFT);
            if (AttackMovePattern.canAttackAt(board, boardPiece, lAttackDest))
                moves.add(new Move.AttackPromoteMove(src, lAttackDest));

            Pos rAttackDest = boardPiece.calcDestination(Dir.UP_RIGHT);
            if (AttackMovePattern.canAttackAt(board, boardPiece, rAttackDest))
                moves.add(new Move.AttackPromoteMove(src, rAttackDest));

            return moves;
        }
    }

    /**
     * Move pattern for king right castling.
     */
    public static final class RightCastlingMovePattern extends MovePattern {
        private static final CastlingMoveChecker checker = new CastlingMoveChecker(
                Dir.RIGHT,
                Dir.KING_RIGHT_ROOK);

        @Override
        public Collection<Move> populateMoves(BoardMatrix board, Pos src) {
            ArrayList<Move> moves = new ArrayList<>();
            if (checker.canCastle(board, src)) {
                moves.add(new Move.RightCastlingMove(src, src.offset(Dir.KING_RIGHT_CASTLING)));
                moves.add(new Move.RightCastlingMove(src, src.offset(Dir.KING_RIGHT_ROOK)));
            }

            return moves;
        }
    }

    /**
     * Move pattern for king left castling.
     */
    public static final class LeftCastlingMovePattern extends MovePattern {
        private static final CastlingMoveChecker checker = new CastlingMoveChecker(
                Dir.LEFT,
                Dir.KING_LEFT_ROOK);

        @Override
        public Collection<Move> populateMoves(BoardMatrix board, Pos src) {
            ArrayList<Move> moves = new ArrayList<>();
            if (checker.canCastle(board, src)) {
                moves.add(new Move.LeftCastlingMove(src, src.offset(Dir.KING_LEFT_CASTLING)));
                moves.add(new Move.LeftCastlingMove(src, src.offset(Dir.KING_LEFT_ROOK)));
            }

            return moves;
        }
    }

    /**
     * General logic for castling. Checks if castling available in certain direction.
     */
    public static final class CastlingMoveChecker {
        private final Dir movingDir;
        private final Dir rookDir;

        public CastlingMoveChecker(Dir movingDir, Dir rookDir) {
            this.movingDir = movingDir;
            this.rookDir = rookDir;
        }

        /**
         * Checks if castling available.
         * @param board for calculation
         * @param src position (start from)
         * @return boolean, True if available
         */
        public boolean canCastle(BoardMatrix board, Pos src) {
            BoardPiece boardPiece = board.getAt(src);
            if (boardPiece == BoardPiece.Empty)
                throw new RuntimeException(String.format("Illegal move source, piece not exists at: %s", src.toString()));

            Piece kingPiece = boardPiece.getPiece();
            if (kingPiece.getPieceType() != PieceType.KING)
                throw new RuntimeException(String.format("Illegal move source, piece is not a King at: %s", src.toString()));

            if (kingPiece.isMoved())
                return false;

            Pos rookPos = boardPiece.calcDestination(rookDir);
            if (rookPos == Pos.OUT_OF_BOARD)
                return false;

            BoardPiece rookBoardPiece = board.getAt(rookPos);
            if (rookBoardPiece == BoardPiece.Empty)
                return false;

            Piece rookPiece = rookBoardPiece.getPiece();
            if (rookPiece.getPieceType() != PieceType.ROOK
                    || kingPiece.getAlliance() != rookPiece.getAlliance()
                    || rookPiece.isMoved())
                return false;

            for (Pos dest = src.offset(movingDir); dest != src.offset(rookDir); dest = dest.offset(movingDir)) {
                if (!JumpMovePattern.canMoveTo(board, dest))
                    return false;
            }

            return true;
        }
    }
}
