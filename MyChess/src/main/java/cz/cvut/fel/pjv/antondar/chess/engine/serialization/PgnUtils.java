package cz.cvut.fel.pjv.antondar.chess.engine.serialization;

import com.google.gson.GsonBuilder;
import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.Game;
import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.GameConfig;

import com.google.gson.Gson;

import java.io.File;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Utility class for game serialization.
 */
public class PgnUtils {
    final static Logger logger =
            LogManager.getLogManager().getLogger(PgnUtils.class.getName());
    /**
     * Serializing game to file
     * @param file for data save
     * @param gameConfig saving game config, contains game options, meta and board pattern for custom board
     * @param game for saving, contains timers and move history
     */
    public static void write(File file, GameConfig gameConfig, Game game) throws Exception {
        GameSerializableData ser = new GameSerializableData(
                SerializationUtils.alliance2Str(gameConfig.getStartPlayerAlliance()),
                SerializationUtils.gameMode2Str(gameConfig.getGameMode()),
                SerializationUtils.alliance2Str(gameConfig.getAiPlayerAlliance()),
                gameConfig.getTimeLimit().toMillis(),
                gameConfig.getGameMeta(),
                BoardSerializableData.fromBoardMatrix(gameConfig.getBoardPattern()),
                game.getWhitePlayerTimer().getTime().toMillis(),
                game.getBlackPlayerTimer().getTime().toMillis(),
                MoveCommandSerializableData.fromHistory(game.getMoveHistory())
        );

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonString = gson.toJson(ser);
        //GameSerializableData deser = gson.fromJson(jsonString, GameSerializableData.class);

        try (var pw = new PrintWriter(file)) {
            pw.println(jsonString);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Cannot save to file", e);
            //throw new Exception("Cannot save to file", e);
        }
    }
}
