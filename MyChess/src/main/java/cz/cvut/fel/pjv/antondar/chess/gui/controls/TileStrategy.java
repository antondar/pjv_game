package cz.cvut.fel.pjv.antondar.chess.gui.controls;

import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.board.Pos;
import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.Game;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.MoveCommand;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.Piece;
import cz.cvut.fel.pjv.antondar.chess.gui.controls.stash.PieceStashPane;
import javafx.beans.property.SimpleObjectProperty;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Strategy for move processing.
 */
public abstract class TileStrategy {
    /**
     * Resolving is move legal.
     * @param source container (move from)
     * @param target container (move to)
     * @return boolean - is move legal
     */
    public abstract Boolean canMoveHere(PieceContainer source, PieceContainer target);

    /**
     * Performing move.
     * @param source container (move from)
     * @param target container (move to)
     */
    public abstract void performMove(PieceContainer source, PieceContainer target);

    /**
     * Strategy for game play.
     */
    public static class GameTileStrategy extends TileStrategy {
        private final Game game;

        public GameTileStrategy(Game game) {
            this.game = game;
        }

        @Override
        public Boolean canMoveHere(PieceContainer source, PieceContainer target) {
            MoveCommand command = new MoveCommand(Pos.At(source.getTileIndex()), Pos.At(target.getTileIndex()));
            return game.canMove(command);
        }

        @Override
        public void performMove(PieceContainer source, PieceContainer target) {
            MoveCommand command = new MoveCommand(Pos.At(source.getTileIndex()), Pos.At(target.getTileIndex()));
            game.move(command);
        }
    }

    /**
     * Strategy for board configuring.
     */
    public static class ConfigureBoardTileStrategy extends TileStrategy {
        private final SimpleObjectProperty<BoardMatrix> board;
        private Logger logger = Logger.getLogger(ConfigureBoardTileStrategy.class.getName());

        public ConfigureBoardTileStrategy(SimpleObjectProperty<BoardMatrix> board) {
            this.board = board;
        }

        @Override
        public Boolean canMoveHere(PieceContainer source, PieceContainer target) {
            Pos sourcePos = Pos.At(source.getTileIndex());
            Pos targetPos = Pos.At(target.getTileIndex());
            return sourcePos != Pos.OUT_OF_BOARD && targetPos != Pos.OUT_OF_BOARD;
        }

        @Override
        public void performMove(PieceContainer source, PieceContainer target) {
            Pos targetPos = Pos.At(target.getTileIndex());
            Pos sourcePos = Pos.At(source.getTileIndex());
            if (sourcePos == Pos.OUT_OF_BOARD || targetPos == Pos.OUT_OF_BOARD) {
                logger.log(Level.INFO, "Can't set piece: out of board");
//                System.out.println(String.format("Can't set piece: out of board"));
                return;
            }

            logger.log(Level.INFO, ()->String.format("Setting at %s", targetPos));
//            System.out.println(String.format("Setting at %s", targetPos));
            BoardMatrix oldBoard = board.get();
            BoardMatrix newBoard = oldBoard.clone();

            Piece piece;
            if (source.isPopulate()) {
                piece = PieceStashPane.getPiece(source.getTileIndex());
                newBoard.setAt(targetPos, piece);
            } else {
                piece = newBoard.getAt(sourcePos).getPiece();
                newBoard.killAt(sourcePos);
                newBoard.setAt(targetPos, piece);
            }

            board.set(newBoard);
            logger.log(Level.INFO, ()->String.format("Setted %s at %s", piece.getPieceType().toString(), targetPos));
//            System.out.println(String.format("Setted %s at %s", piece.getPieceType().toString(), targetPos));
        }
    }

    /**
     * Default nothing-to-do strategy.
     */
    public static class NullTileStrategy extends TileStrategy {
        @Override
        public Boolean canMoveHere(PieceContainer source, PieceContainer target) {
            return true;
        }

        @Override
        public void performMove(PieceContainer source, PieceContainer target) {}
    }

    /**
     * Denied all strategy.
     */
    public static class DeniedTileStrategy extends TileStrategy {
        @Override
        public Boolean canMoveHere(PieceContainer source, PieceContainer target) {
            return false;
        }

        @Override
        public void performMove(PieceContainer source, PieceContainer target) {}
    }
}
