package cz.cvut.fel.pjv.antondar.chess.engine.pieces;

import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.board.Dir;
import cz.cvut.fel.pjv.antondar.chess.engine.board.Pos;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.MovePattern;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;

/**
 * Represents class of Bishop
 */
public class Bishop extends Piece {
    // Rules for move
    private static final MovePattern[] movePatterns = {
            // Scale jump moves
            new MovePattern.ScaleJumpMovePattern(Dir.UP_LEFT, BoardMatrix.BOARD_SIZE),
            new MovePattern.ScaleJumpMovePattern(Dir.UP_RIGHT, BoardMatrix.BOARD_SIZE),
            new MovePattern.ScaleJumpMovePattern(Dir.DOWN_RIGHT, BoardMatrix.BOARD_SIZE),
            new MovePattern.ScaleJumpMovePattern(Dir.DOWN_LEFT, BoardMatrix.BOARD_SIZE),

            // Scale attack moves
            new MovePattern.ScaleAttackMovePattern(Dir.UP_LEFT, BoardMatrix.BOARD_SIZE),
            new MovePattern.ScaleAttackMovePattern(Dir.UP_RIGHT, BoardMatrix.BOARD_SIZE),
            new MovePattern.ScaleAttackMovePattern(Dir.DOWN_RIGHT, BoardMatrix.BOARD_SIZE),
            new MovePattern.ScaleAttackMovePattern(Dir.DOWN_LEFT, BoardMatrix.BOARD_SIZE)
    };

    private Bishop(Alliance alliance, boolean hasMoved) {
        super(PieceType.BISHOP, alliance, hasMoved);
    }

    @Override
    protected MovePattern[] getMovePatterns(Alliance alliance, Pos position) {
        return movePatterns;
    }

    @Override
    public Piece move() { return alliance == Alliance.WHITE ? WhiteMoved : BlackMoved; }

    public static final Bishop White = new Bishop(Alliance.WHITE, false);
    public static final Bishop Black = new Bishop(Alliance.BLACK, false);
    public static final Bishop WhiteMoved = new Bishop(Alliance.WHITE, true);
    public static final Bishop BlackMoved = new Bishop(Alliance.BLACK, true);
}
