package cz.cvut.fel.pjv.antondar.chess.engine.moves;

import cz.cvut.fel.pjv.antondar.chess.engine.board.*;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.*;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;
import lombok.Getter;

import java.util.Collection;

/**
 * Represent abstract move class
 */
public abstract class Move {
    @Getter protected final Pos src;
    @Getter protected final Pos dest;

    /**
     * Creates move from source to destination position
     * @param src source position
     * @param dest destination position
     */
    public Move(final Pos src, final Pos dest) {
        this.src = src;
        this.dest = dest;
    }

    /**
     * Executes move and creating changed game board
     * @param board game board
     * @return new game board
     */
    public abstract BoardMatrix execute(BoardMatrix board);

    /**
     * Checks if move is legal
     * @param board game board
     * @return boolean
     */
    public abstract boolean isLegal(BoardMatrix board);

    /**
     * Checks if move is attack
     * @return boolean
     */
    public abstract boolean isAttack();

    /**
     * Represent default jump move
     */
    public static final class JumpMove extends Move {
        /**
         * Creates jump move from source to destination position
         * @param src source position
         * @param dest destination position
         */
        public JumpMove(Pos src, Pos dest) {
            super(src, dest);
        }

        /**
         * Executes jump move
         * @param board game board
         * @return
         */
        @Override
        public BoardMatrix execute(BoardMatrix board) {
            Piece piece = board.getAt(src).getPiece();
            BoardMatrix newBoard = board.clone();
            //delay a piece on source position
            newBoard.killAt(src);
            // set a piece on destination position
            newBoard.setAt(dest, piece.move());
            newBoard.resetEnPassant();
            return newBoard;
        }

        /**
         * Checks if jump move is legal
         * @param board game board
         * @return bollean
         */
        @Override
        public boolean isLegal(BoardMatrix board) {
            BoardMatrix newBoard = execute(board);
            Piece piece = newBoard.getAt(dest).getPiece();
            // calculates opponent moves
            final Collection<Move> opponentMoves = Calculations.populateMoves(newBoard, piece.getAlliance().opposite());
            // find king on a board
            BoardPiece kingBoardPiece = newBoard.getKing(piece.getAlliance());
            if (kingBoardPiece == BoardPiece.Empty)
                throw new RuntimeException(String.format("Cannot find King piece (%s)", piece.getAlliance()));

            // checks if kind is not on attack. Otherwise impossible to execute current move
            boolean isInCheck = Calculations.isOnAttack(kingBoardPiece.getPosition(), opponentMoves);
            return !isInCheck;
        }

        /**
         * Checks if move is attack (impossible)
         * @return false
         */
        @Override
        public boolean isAttack() {
            return false;
        }
    }

    /**
     * Represent pawn long jump move
     */
    public static final class PawnLongJumpMove extends Move {
        /**
         * Creates pawn long jump move from source to destination position
         * @param src source position
         * @param dest destination position
         */
        public PawnLongJumpMove(Pos src, Pos dest) {
            super(src, dest);
        }

        /**
         * Executes pawn long jump move
         * @param board game board
         * @return
         */
        @Override
        public BoardMatrix execute(BoardMatrix board) {
            Piece piece = board.getAt(src).getPiece();
            BoardMatrix newBoard = board.clone();
            newBoard.killAt(src);
            newBoard.setAt(dest, piece.move());
            newBoard.setEnPassant(dest);
            return newBoard;
        }

        /**
         * Checks if pawn long jump move is legal
         * @param board game board
         * @return bollean
         */
        @Override
        public boolean isLegal(BoardMatrix board) {
            BoardMatrix newBoard = execute(board);
            Piece piece = newBoard.getAt(dest).getPiece();
            final Collection<Move> opponentMoves = Calculations.populateMoves(newBoard, piece.getAlliance().opposite());
            BoardPiece kingBoardPiece = newBoard.getKing(piece.getAlliance());
            if (kingBoardPiece == BoardPiece.Empty)
                throw new RuntimeException(String.format("Cannot find King piece (%s)", piece.getAlliance()));

            boolean isInCheck = Calculations.isOnAttack(kingBoardPiece.getPosition(), opponentMoves);
            return !isInCheck;
        }

        /**
         * Checks if pawn long jump move is attack (impossible)
         * @return false
         */
        @Override
        public boolean isAttack() {
            return false;
        }
    }

    /**
     * Represent attack move
     */
    public static final class AttackMove extends Move {
        /**
         * Creates attack move from source to destination position
         * @param src source position
         * @param dest destination position
         */
        public AttackMove(Pos src, Pos dest) {
            super(src, dest);
        }

        /**
         * Executes attack move
         * @param board game board
         * @return
         */
        @Override
        public BoardMatrix execute(BoardMatrix board) {
            Piece piece = board.getAt(src).getPiece();
            BoardMatrix newBoard = board.clone();
            newBoard.killAt(src);
            newBoard.setAt(dest, piece.move());
            newBoard.resetEnPassant();
            return newBoard;
        }

        /**
         * Checks if attack move is legal
         * @param board game board
         * @return bollean
         */
        @Override
        public boolean isLegal(BoardMatrix board) {
            Piece attackedPiece = board.getAt(dest).getPiece();
            // we can't eat the king
            if (attackedPiece.getPieceType() == PieceType.KING)
                return false;

            BoardMatrix newBoard = execute(board);
            Piece piece = newBoard.getAt(dest).getPiece();

            final Collection<Move> opponentMoves = Calculations.populateMoves(newBoard, piece.getAlliance().opposite());
            BoardPiece kingBoardPiece = newBoard.getKing(piece.getAlliance());
            if (kingBoardPiece == BoardPiece.Empty)
                throw new RuntimeException(String.format("Cannot find King piece (%s)", piece.getAlliance()));

            boolean isInCheck = Calculations.isOnAttack(kingBoardPiece.getPosition(), opponentMoves);
            return !isInCheck;
        }

        /**
         * Checks if is attack
         * @return true
         */
        @Override
        public boolean isAttack() {
            return true;
        }
    }

    /**
     * Represent right castling move
     */
    public static final class RightCastlingMove extends Move {
        private static final CastlingMoveImpl impl = new CastlingMoveImpl(
                Dir.RIGHT,
                Dir.KING_RIGHT_CASTLING,
                Dir.KING_RIGHT_ROOK);

        /**
         * Creates right castling move
         * @param src source position
         * @param dest destination position
         */
        public RightCastlingMove(Pos src, Pos dest) {
            super(src, dest);
        }


        /**
         * Executes right castling move
         * @param board game board
         * @return
         */
        @Override
        public BoardMatrix execute(BoardMatrix board) {
            return impl.execute(src, board);
        }

        /**
         * hecks if right castling move is legal
         * @param board game board
         * @return
         */
        @Override
        public boolean isLegal(BoardMatrix board) {
            return impl.isLegal(src, board);
        }

        /**
         * Checks if right castling move is attack (impossible)
         * @return false
         */
        @Override
        public boolean isAttack() {
            return false;
        }
    }

    /**
     * Represent left castling move
     */
    public static final class LeftCastlingMove extends Move {
        private static final CastlingMoveImpl impl = new CastlingMoveImpl(
                Dir.LEFT,
                Dir.KING_LEFT_CASTLING,
                Dir.KING_LEFT_ROOK);

        /**
         * Creates left castling move
         * @param src source position
         * @param dest destination position
         */
        public LeftCastlingMove(Pos src, Pos dest) {
            super(src, dest);
        }

        /**
         * Executes left castling move
         * @param board game board
         * @return update board
         */
        @Override
        public BoardMatrix execute(BoardMatrix board) {
            return impl.execute(src, board);
        }

        /**
         * Checks if left castling move is legal
         * @param board game board
         * @return boolean
         */
        @Override
        public boolean isLegal(BoardMatrix board) {
            return impl.isLegal(src, board);
        }

        /**
         * Checks if left castling move is attack (impossible)
         * @return false
         */
        @Override
        public boolean isAttack() {
            return false;
        }
    }

    /**
     * General implementation for castling move logic, depends on direction
     */
    public static final class CastlingMoveImpl {
        private final Dir movingDir;
        private final Dir destDir;
        private final Dir rookDir;

        public CastlingMoveImpl(Dir movingDir, Dir destDir, Dir rookDir) {
            this.movingDir = movingDir;
            this.destDir = destDir;
            this.rookDir = rookDir;
        }

        /**
         * Executes castling move
         * @param board game board
         * @return update board
         */
        public BoardMatrix execute(Pos src, BoardMatrix board) {
            Pos oldRookPos = src.offset(rookDir);
            Piece kingPiece = board.getAt(src).getPiece();
            Piece rookPiece = board.getAt(oldRookPos).getPiece();

            BoardMatrix newBoard = board.clone();
            Pos newRookPos = src.offset(movingDir);
            Pos newKingPos = src.offset(destDir);

            newBoard.killAt(src);
            newBoard.killAt(oldRookPos);
            newBoard.setAt(newRookPos, rookPiece.move());
            newBoard.setAt(newKingPos, kingPiece.move());
            newBoard.resetEnPassant();
            return newBoard;
        }

        /**
         * Checks if castling move is legal
         * @param board game board
         * @return boolean
         */
        public boolean isLegal(Pos src, BoardMatrix board) {
            BoardMatrix newBoard = execute(src, board);
            BoardPiece kingBoardPiece = newBoard.getAt(src.offset(destDir));
            Piece kingPiece = kingBoardPiece.getPiece();
            Alliance playerAlliance = kingPiece.getAlliance();

            for (Pos at = src; at != src.offset(rookDir); at = at.offset(movingDir)) {
                newBoard.setAt(at, kingPiece);
            }

            final Collection<Move> opponentMoves = Calculations.populateMoves(newBoard, playerAlliance.opposite());

            for (Pos at = src; at != src.offset(rookDir); at = at.offset(movingDir)) {
                boolean isOnAttack = Calculations.isOnAttack(at, opponentMoves);
                if (isOnAttack)
                    return false;
            }

            return true;
        }
    }

    /**
     * En Passant move realization (killing previous long jump moved pawn)
     */
    public static final class EnPassantMove extends Move {
        public EnPassantMove(Pos src, Pos dest) {
            super(src, dest);
        }

        @Override
        public BoardMatrix execute(BoardMatrix board) {
            Piece piece = board.getAt(src).getPiece();
            BoardMatrix newBoard = board.clone();
            newBoard.killAt(src);
            newBoard.killAt(board.getEnPassant());
            newBoard.setAt(dest, piece.move());
            newBoard.resetEnPassant();
            return newBoard;
        }

        @Override
        public boolean isLegal(BoardMatrix board) {
            BoardMatrix newBoard = execute(board);
            Piece piece = newBoard.getAt(dest).getPiece();
            final Collection<Move> opponentMoves = Calculations.populateMoves(newBoard, piece.getAlliance().opposite());
            BoardPiece kingBoardPiece = newBoard.getKing(piece.getAlliance());
            if (kingBoardPiece == BoardPiece.Empty)
                throw new RuntimeException(String.format("Cannot find King piece (%s)", piece.getAlliance()));

            boolean isInCheck = Calculations.isOnAttack(kingBoardPiece.getPosition(), opponentMoves);
            return !isInCheck;
        }

        @Override
        public boolean isAttack() {
            return true;
        }
    }

    /**
     * Promote move realization (pawn is on the last board row) for simple move promote
     */
    public static class PromoteMove extends Move {
        private Piece promotePiece = null;

        public PromoteMove(Pos src, Pos dest) {
            super(src, dest);
        }

        /**
         * setting piece used for promote, usages are not currently implemented
         * @param piece for promote
         */
        public void setPromotePieceType(Piece piece) {
            PieceType pieceType = piece.getPieceType();

            if (pieceType != PieceType.BISHOP
                    && pieceType != PieceType.ROOK
                    && pieceType != PieceType.KNIGHT
                    && pieceType != PieceType.QUEEN)
                throw new RuntimeException(String.format("Illegal piece type for promote: %s", pieceType));

            promotePiece = piece;
        }

        @Override
        public BoardMatrix execute(BoardMatrix board) {
            BoardMatrix newBoard = board.clone();
            newBoard.killAt(src);
            Piece promotePiece = getPromotePiece(board);
            newBoard.setAt(dest, promotePiece.move());
            newBoard.resetEnPassant();
            return newBoard;
        }

        public Piece getPromotePiece(BoardMatrix board) {
            if (this.promotePiece != null)
                return this.promotePiece;

            Piece pawn = board.getAt(src).getPiece();
            return pawn.getAlliance().isWhite() ? Queen.WhiteMoved : Queen.BlackMoved;
        }

        @Override
        public boolean isLegal(BoardMatrix board) {
            BoardMatrix newBoard = execute(board);
            Piece piece = newBoard.getAt(dest).getPiece();
            final Collection<Move> opponentMoves = Calculations.populateMoves(newBoard, piece.getAlliance().opposite());
            BoardPiece kingBoardPiece = newBoard.getKing(piece.getAlliance());
            if (kingBoardPiece == BoardPiece.Empty)
                throw new RuntimeException(String.format("Cannot find King piece (%s)", piece.getAlliance()));

            boolean isInCheck = Calculations.isOnAttack(kingBoardPiece.getPosition(), opponentMoves);
            return !isInCheck;
        }

        @Override
        public boolean isAttack() {
            return false;
        }
    }

    /**
     * Promote move with attack
     */
    public static final class AttackPromoteMove extends PromoteMove {
        public AttackPromoteMove(Pos src, Pos dest) {
            super(src, dest);
        }

        @Override
        public boolean isAttack() {
            return true;
        }
    }

    public static final class EmptyMove extends Move {
        private EmptyMove() {
            super(Pos.OUT_OF_BOARD, Pos.OUT_OF_BOARD);
        }

        @Override
        public BoardMatrix execute(BoardMatrix board) {
            throw new RuntimeException("Empty move isn't supported!");
        }

        @Override
        public boolean isLegal(BoardMatrix board) {
            return false;
        }

        @Override
        public boolean isAttack() {
            return false;
        }
    }

    /**
     * Predefined constant, move without functionality
     */
    public static final Move EMPTY_MOVE = new EmptyMove();
}
