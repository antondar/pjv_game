package cz.cvut.fel.pjv.antondar.chess.engine.gameplay.thread;

import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.Game;

import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Represent game thread
 */
public abstract class GameThread extends Thread {
    final static Logger logger = Logger.getLogger(GameThread.class.getName());
    protected final Game game;

    /**
     * Creates game thread
     * @param game
     */
    public GameThread(Game game) {
        this.game = game;
    }

    /**
     * Starts game thread
     */
    public void run() {
        logger.log(Level.INFO,"Game thread started");
        //System.out.println("Game thread started");

        try {
            process();
        } catch (Exception e) {
            logger.log(Level.WARNING, String.valueOf(e.getStackTrace()));
            //System.out.println(e.getStackTrace());
        }
    }

    // Process game thread until game state is not Finished
    private void process() throws InterruptedException {
        while (game.getState() != Game.GameState.Finished) {
            iterationSafe();
            Thread.sleep(100); // sleep 100ms
        }
    }

    // Actualizes game state and executes abstract iteration
    private void iterationSafe() {
        try {
            game.actualizeState();
            iteration();
            game.actualizeState();
        } catch (Exception e) {
            logger.log(Level.WARNING, String.valueOf(e.getStackTrace()));
            //System.out.println(e.getStackTrace());
        }
    }

    // Defines specific logic for children classes
    protected abstract void iteration();
}
