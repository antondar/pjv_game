package cz.cvut.fel.pjv.antondar.chess.gui.controllers;

import cz.cvut.fel.pjv.antondar.App;
import cz.cvut.fel.pjv.antondar.chess.gui.controls.board.ChessPane;
import cz.cvut.fel.pjv.antondar.chess.gui.ViewModelManager;
import cz.cvut.fel.pjv.antondar.chess.gui.models.GameViewModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents controller for game window, managing FXML game view
 */
public class GameController {
    private GameViewModel gameModel = ViewModelManager.getInstance().getGame();
    private Logger logger = Logger.getLogger(GameController.class.getName());

    // Labels
    @FXML
    Label gameStateLabel;

    @FXML
    Label gameResultLabel;

    @FXML
    Label whitePlayerLabel;

    @FXML
    Label blackPlayerLabel;

    // Buttons
    @FXML
    Button savePgnButton;

    @FXML
    Button startButton;

    @FXML
    Button restartButton;

    @FXML
    Button resignButton;

    @FXML
    Button backToMenuButton;

    // Pane
    @FXML
    ChessPane chessPane;

    /**
     * Post-creation initialization.
     * Sets processing strategy for movement actions of board.
     * Binds view-model board with control board, binds timers, state and result fields.
     */
    @FXML
    public void initialize() {
        logger.log(Level.INFO, "Game view controller initialization");
        //System.out.println("Game view controller initialization");
        whitePlayerLabel.textProperty().bind(gameModel.getWhitePlayerTimer());
        blackPlayerLabel.textProperty().bind(gameModel.getBlackPlayerTimer());
        gameStateLabel.textProperty().bind(gameModel.getState());
        gameResultLabel.textProperty().bind(gameModel.getResult());
        chessPane.setTileStrategyContainer(gameModel.getStrategyContainer());
        chessPane.getBoardProperty().bind(gameModel.getBoard());
    }

    /**
     * Handler for save pgn button click
     */
    @FXML
    private void savePgn(ActionEvent ae) throws IOException {
        Node source = (Node) ae.getSource();
        Window stage = source.getScene().getWindow();

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Portable Game Notation", "*.pgn"));
        File file = fileChooser.showSaveDialog(stage);

        if (file != null) {
            logger.log(Level.INFO, "File was chosen");
            //System.out.println("File was chosen");
            try {
                gameModel.saveGame(file);
            } catch (Exception e) {
                logger.log(Level.INFO, "Error save game");
                logger.log(Level.SEVERE,"Cannot save to file", e.getStackTrace());
                //System.out.println("Error save game");
                //System.out.println(e.getStackTrace());
                App.showModalAlert(String.format("Cannot save game to file. Reason: %s", e.getMessage()));
            }
        }
    }

    /**
     * Handler for restart button click
     */
    @FXML
    private void restartGame(ActionEvent ae) throws IOException {
        gameModel.restart();
    }

    /**
     * Handler for start game button click
     */
    @FXML
    private void startGame(ActionEvent ae) throws IOException {
        gameModel.start();
    }

    /**
     * Handler for resign game button click
     */
    @FXML
    private void resignGame(ActionEvent ae) throws IOException {
        gameModel.resign();
    }

    /**
     * Handler for back to menu button click
     */
    @FXML
    private void backToMenu(ActionEvent ae) throws IOException {
        gameModel.terminateGame();
        App.setRoot("MainWindow");
    }
}
