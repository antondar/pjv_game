package cz.cvut.fel.pjv.antondar.chess.engine.board;

import cz.cvut.fel.pjv.antondar.chess.engine.moves.Move;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.BoardPiece;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.PieceType;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents calculation different types of moves, utility class
 */
public class Calculations {


    private Calculations() { }

    /**
     * Collects all moves for pieces on a board
     * @param board is a current game board
     * @param pieces is collection of all pieces on the board
     * @return collection of all possible moves for pieces
     */
    public static Collection<Move> populateMoves(BoardMatrix board, Collection<BoardPiece> pieces) {
        return pieces
                .stream()
                .flatMap(piece -> piece.populateMoves(board).stream())
                .collect(Collectors.toList());
    }

    /**
     * Collects all moves for the requested alliance
     * @param board is a current game board
     * @param alliance is black or white alliance
     * @return collection of all possible moves for the requested alliance
     */
    public static Collection<Move> populateMoves(BoardMatrix board, Alliance alliance) {
        return board.getPieces(alliance)
                .stream()
                .flatMap(piece -> piece.populateMoves(board).stream())
                .collect(Collectors.toList());
    }

    /**
     * Checks if King is on attack
     * @param boardMatrix is a current game board
     * @param alliance is black or white alliance
     * @param moves is collection of all possible moves
     * @return boolean
     */
    public static boolean isCheck(BoardMatrix boardMatrix, Alliance alliance, Collection<Move> moves) {
        BoardPiece boardPiece = boardMatrix.getKing(alliance);
        if (boardPiece == BoardPiece.Empty)
            return false;

        return isOnAttack(boardPiece.getPosition(), moves);
    }

    /**
     * Check if a tile is on attack
     * @param position is tile's coordinates
     * @param moves is collection of all possible moves
     * @return boolean
     */
    public static boolean isOnAttack(Pos position, Collection<Move> moves) {
        return !calculateAttacksOnTile(position, moves).isEmpty();
    }

    /**
     * Calculate all moves which destination is @param position
     * @param position is tile's coordinates
     * @param moves is collection of all possible moves
     * @return Collection of moves is attack moves on tile
     */
    public static Collection<Move> calculateAttacksOnTile(Pos position, Collection<Move> moves) {
        final List<Move> attackMoves = new ArrayList<>();
        for (final Move move : moves) {
            if(position == move.getDest() && move.isAttack()) {
                attackMoves.add(move);
            }
        }
        return attackMoves;
    }

    public static boolean checkBoardValid(BoardMatrix board) {
        Collection<BoardPiece> whitePieces = board.getPieces(Alliance.WHITE);
        if (countKings(whitePieces) != 1)
            return false;

        Collection<BoardPiece> blackPieces = board.getPieces(Alliance.BLACK);
        if (countKings(blackPieces) != 1)
            return false;

        BoardPiece whiteKing = board.getKing(Alliance.WHITE);
        BoardPiece blackKing = board.getKing(Alliance.BLACK);

        return !whiteKing.getPosition().isNearby(blackKing.getPosition());
    }

    private static int countKings(Collection<BoardPiece> pieces) {
        return (int)pieces.stream()
            .filter(p -> p != BoardPiece.Empty && p.getPiece().getPieceType() == PieceType.KING)
            .count();
    }
}
