package cz.cvut.fel.pjv.antondar.chess.gui.controllers;

import cz.cvut.fel.pjv.antondar.App;
import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.Game;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;
import cz.cvut.fel.pjv.antondar.chess.gui.ViewModelManager;
import cz.cvut.fel.pjv.antondar.chess.gui.models.GameSetupViewModel;

import cz.cvut.fel.pjv.antondar.chess.gui.models.GameViewModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.io.IOException;

/**
 * Represent controller for setup window, managing FXML game setup view
 */
public class GameSetupController {
    private final GameSetupViewModel gameSetupModel = ViewModelManager.getInstance().getGameSetup();
    private final GameViewModel gameModel = ViewModelManager.getInstance().getGame();

    // Game mode
    @FXML
    private ToggleGroup gameModeGroup;
    @FXML
    private RadioButton pvpRadioButton;
    @FXML
    private RadioButton pveRadioButton;

    // AI color
    @FXML
    private ToggleGroup aiColorGroup;
    @FXML
    private RadioButton aiWhiteButton;
    @FXML
    private RadioButton aiBlackButton;

    // Start player color
    @FXML
    private ToggleGroup startColorGroup;
    @FXML
    private RadioButton startWhiteButton;
    @FXML
    private RadioButton startBlackButton;

    // Board Type
    @FXML
    private ToggleGroup boardTypeGroup;
    @FXML
    private RadioButton standardBoardButton;
    @FXML
    private RadioButton customBoardButton;

    // Time limit
    @FXML
    private TextField timeLimitMinutes;

    // Game meta
    @FXML
    private CheckBox metainfoCheckBox;
    @FXML
    private TextField annotatorField;
    @FXML
    private TextField wpNameField;
    @FXML
    private TextField bpNameField;
    @FXML
    private TextField gameNameField;
    @FXML
    private TextField gamePlaceField;
    @FXML
    private VBox gameMetaInfoBox;

    /**
     * Post-creation initialization.
     * Binds view-model properties with text fields and radio buttons of game setup window.
     */
    @FXML
    public void initialize() {
        pvpRadioButton.setSelected(gameSetupModel.getGameMode() == Game.Mode.PVP);
        pveRadioButton.setSelected(gameSetupModel.getGameMode() == Game.Mode.PVE);
        aiWhiteButton.setSelected(gameSetupModel.getAiPlayerAlliance() == Alliance.WHITE);
        aiBlackButton.setSelected(gameSetupModel.getAiPlayerAlliance() == Alliance.BLACK);
        startWhiteButton.setSelected(gameSetupModel.getStartPlayerAlliance() == Alliance.WHITE);
        startBlackButton.setSelected(gameSetupModel.getStartPlayerAlliance() == Alliance.BLACK);
        standardBoardButton.setSelected(!gameSetupModel.isCustomBoard());
        customBoardButton.setSelected(gameSetupModel.isCustomBoard());

        gameModeGroup.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {
            RadioButton button = (RadioButton) gameModeGroup.getSelectedToggle();
            gameSetupModel.setGameMode(button == pvpRadioButton ? Game.Mode.PVP : Game.Mode.PVE);
        });

        aiColorGroup.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {
            RadioButton button = (RadioButton) aiColorGroup.getSelectedToggle();
            gameSetupModel.setAiPlayerAlliance(button == aiWhiteButton ? Alliance.WHITE : Alliance.BLACK);
        });

        startColorGroup.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {
            RadioButton button = (RadioButton) startColorGroup.getSelectedToggle();
            gameSetupModel.setStartPlayerAlliance(button == startWhiteButton ? Alliance.WHITE : Alliance.BLACK);
        });

        boardTypeGroup.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {
            RadioButton button = (RadioButton) boardTypeGroup.getSelectedToggle();
            gameSetupModel.setCustomBoard(button == customBoardButton);
        });

        timeLimitMinutes.textProperty().bindBidirectional(gameSetupModel.getTimeLimitMinutes());

        annotatorField.textProperty().bindBidirectional(gameSetupModel.getGameMetaViewModel().getAnnotator());
        wpNameField.textProperty().bindBidirectional(gameSetupModel.getGameMetaViewModel().getWhitePlayerName());
        bpNameField.textProperty().bindBidirectional(gameSetupModel.getGameMetaViewModel().getBlackPlayerName());
        gameNameField.textProperty().bindBidirectional(gameSetupModel.getGameMetaViewModel().getGameName());
        gamePlaceField.textProperty().bindBidirectional(gameSetupModel.getGameMetaViewModel().getGamePlace());
    }

    /**
     * Handler for add game meta checkbox click.
     * If selected, shows additional fields, otherwise hides it.
     */
    @FXML
    private void handleMetaCheckbox() {
        gameMetaInfoBox.setManaged(metainfoCheckBox.isSelected());
        gameMetaInfoBox.setVisible(metainfoCheckBox.isSelected());
    }

    /**
     * Handler for start game button click.
     */
    @FXML
    private void startGame(ActionEvent ae) throws IOException {
        // Going to game or custom board configuration
        if (standardBoardButton.isSelected()) {
            gameSetupModel.saveGameConfig();
            gameModel.recreateGame();
            App.setRoot("Game");
        } else {
            App.setRoot("BoardConfigurator");
        }
    }
}
