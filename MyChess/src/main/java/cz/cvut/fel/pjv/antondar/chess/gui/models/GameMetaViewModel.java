package cz.cvut.fel.pjv.antondar.chess.gui.models;

import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.GameMeta;
import javafx.beans.property.SimpleStringProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * View-model for game configuration window meta info
 */
public class GameMetaViewModel {
    /**
     * Defines property for binding with annotator field
     */
    @Getter @Setter
    private SimpleStringProperty annotator = new SimpleStringProperty("MyChess Client 1.0 (antondar@fel.cvut.cz)");

    /**
     * Defines property for binding with white player name field
     */
    @Getter @Setter
    private SimpleStringProperty whitePlayerName = new SimpleStringProperty("White");

    /**
     * Defines property for binding with black player name field
     */
    @Getter @Setter
    private SimpleStringProperty blackPlayerName = new SimpleStringProperty("Black");

    /**
     * Defines property for binding with game name field
     */
    @Getter @Setter
    private SimpleStringProperty gameName = new SimpleStringProperty("Local Game");

    /**
     * Defines property for binding with game place field
     */
    @Getter @Setter
    private SimpleStringProperty gamePlace = new SimpleStringProperty("Czech Republic");

    /**
     * Creates meta data object from engine logic
     * @return meta data
     */
    public GameMeta createMeta() {
        return new GameMeta(
                annotator.get(),
                gameName.get(),
                gamePlace.get(),
                whitePlayerName.get(),
                blackPlayerName.get()
        );
    }
}
