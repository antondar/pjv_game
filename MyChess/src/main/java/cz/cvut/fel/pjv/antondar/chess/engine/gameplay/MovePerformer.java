package cz.cvut.fel.pjv.antondar.chess.engine.gameplay;

import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardBuilder;
import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.Move;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.MoveCommand;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.MoveHistory;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.BoardPiece;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.Piece;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;
import cz.cvut.fel.pjv.antondar.chess.engine.board.Board;

import java.util.Collection;
import java.util.Optional;

/**
 * Performer for game moves. Contains logic for executing move commands.
 * Manages game moves history.
 */
public class MovePerformer {
    /**
     * Performs move, saves it to history
     * @param board for perform
     * @param history of moves
     * @param command for current move
     * @return new board
     */
    public Board perform(Board board, MoveHistory history, MoveCommand command) {
        if (!isCurPlayerMove(board, command))
            throw new RuntimeException(String.format("Move is unavailable for current player: %s", command.toString()));

        Move move = board.findMove(command);
        if (move == Move.EMPTY_MOVE)
            throw new RuntimeException(String.format("Legal move not exists: %s", command.toString()));

        return perform(board, history, move);
    }

    /**
     * Checks move availability
     * @param board for perform
     * @param command for current move
     * @return boolean, true if move available
     */
    public boolean canMove(Board board, MoveCommand command) {
        if (!isCurPlayerMove(board, command))
            return false;

        Move move = board.findMove(command);
        return move == Move.EMPTY_MOVE ? false : true;
    }

    private boolean isCurPlayerMove(Board board, MoveCommand command) {
        BoardPiece boardPiece = board.getBoard().getAt(command.getFrom());
        if (boardPiece == BoardPiece.Empty)
            return false;

        Piece piece = boardPiece.getPiece();
        return board.getCurrentPlayer().getAlliance() == piece.getAlliance();
    }

    /**
     * Performs random move by certain alliance
     * @param board for perform
     * @param history of moves
     * @param alliance of player to move
     * @return new board
     */
    public Board randomMove(Board board, MoveHistory history, Alliance alliance) {
        Collection<Move> moves = board.getMoves(alliance);
        Move move = getRandomMove(moves);
        return perform(board, history, move);
    }

    private Move getRandomMove(Collection<Move> moves) {
        Optional<Move> optionalMove = moves.stream()
                .skip((int) (moves.size() * Math.random()))
                .findFirst();

        if (optionalMove.isEmpty())
            throw new RuntimeException("Cannot find available move to perform random move");

        return optionalMove.get();
    }

    private Board perform(Board board, MoveHistory history, Move move) {
        BoardMatrix newBoardMatrix = move.execute(board.getBoard());
        final BoardBuilder builder = new BoardBuilder(newBoardMatrix);
        builder.setMoveMaker(board.getCurrentPlayer().getAlliance().opposite());
        Board newBoard = builder.build();
        history.add(newBoard, move);
        return newBoard;
    }
}
