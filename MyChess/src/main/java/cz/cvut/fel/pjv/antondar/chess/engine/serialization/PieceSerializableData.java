package cz.cvut.fel.pjv.antondar.chess.engine.serialization;

import cz.cvut.fel.pjv.antondar.chess.engine.pieces.BoardPiece;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.Piece;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.PieceType;
import lombok.Getter;
import lombok.Setter;

/**
 * Defines class for piece serializable type, acceptable by serializer.
 */
public class PieceSerializableData {
    @Getter @Setter
    private final PositionSerializableData pos;

    @Getter @Setter
    private final boolean moved;

    @Getter @Setter
    private final String pieceType;

    @Getter @Setter
    private final String alliance;

    public PieceSerializableData(PositionSerializableData pos, boolean moved, String pieceType, String alliance) {
        this.pos = pos;
        this.moved = moved;
        this.pieceType = pieceType;
        this.alliance = alliance;
    }

    /**
     * Constructs from engine logic board piece class
     */
    public static PieceSerializableData fromBoardPiece(BoardPiece boardPiece) {
        if (boardPiece == BoardPiece.Empty)
            throw new RuntimeException("Illegal logic, can't to serialize empty board piece");

        Piece piece = boardPiece.getPiece();
        return new PieceSerializableData(
                PositionSerializableData.fromPos(boardPiece.getPosition()),
                piece.isMoved(),
                pieceTypeToStr(piece.getPieceType()),
                SerializationUtils.alliance2Str(piece.getAlliance()));
    }

    private static String pieceTypeToStr(PieceType pieceType) {
        return pieceType.name();
    }
}
