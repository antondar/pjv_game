package cz.cvut.fel.pjv.antondar.chess.engine.serialization;

import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.GameMeta;
import lombok.Getter;
import lombok.Setter;

/**
 * Defines class for game serializable type, acceptable by serializer.
 */
public class GameSerializableData {
    @Getter @Setter
    private final String startPlayerAlliance;

    @Getter @Setter
    private final String gameMode;

    @Getter @Setter
    private final String aiPlayerAlliance;

    @Getter @Setter
    private final long timeLimitMillis;

    @Getter @Setter
    private final GameMeta gameMeta;

    @Getter @Setter
    private final BoardSerializableData boardPattern;

    @Getter @Setter
    private final long whitePlayerTimerMillis;

    @Getter @Setter
    private final long blackPlayerTimerMillis;

    @Getter @Setter
    private final MoveCommandSerializableData[] moves;

    public GameSerializableData(String startPlayerAlliance, String gameMode, String aiPlayerAlliance, long timeLimitMillis,
                                GameMeta gameMeta, BoardSerializableData boardPattern, long whitePlayerTimerMillis,
                                long blackPlayerTimerMillis, MoveCommandSerializableData[] moves) {
        this.startPlayerAlliance = startPlayerAlliance;
        this.gameMode = gameMode;
        this.aiPlayerAlliance = aiPlayerAlliance;
        this.timeLimitMillis = timeLimitMillis;
        this.gameMeta = gameMeta;
        this.boardPattern = boardPattern;
        this.whitePlayerTimerMillis = whitePlayerTimerMillis;
        this.blackPlayerTimerMillis = blackPlayerTimerMillis;
        this.moves = moves;
    }
}
