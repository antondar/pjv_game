package cz.cvut.fel.pjv.antondar.chess.engine.pieces;

import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.board.Dir;
import cz.cvut.fel.pjv.antondar.chess.engine.board.Pos;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.MovePattern;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;

/**
 * Represents class of Rook
 */
public class Rook extends Piece {
    private static final MovePattern[] movePatterns = {
            // Scale jump moves
            new MovePattern.ScaleJumpMovePattern(Dir.UP, BoardMatrix.BOARD_SIZE),
            new MovePattern.ScaleJumpMovePattern(Dir.DOWN, BoardMatrix.BOARD_SIZE),
            new MovePattern.ScaleJumpMovePattern(Dir.LEFT, BoardMatrix.BOARD_SIZE),
            new MovePattern.ScaleJumpMovePattern(Dir.RIGHT, BoardMatrix.BOARD_SIZE),

            // Scale attack moves
            new MovePattern.ScaleAttackMovePattern(Dir.UP, BoardMatrix.BOARD_SIZE),
            new MovePattern.ScaleAttackMovePattern(Dir.DOWN, BoardMatrix.BOARD_SIZE),
            new MovePattern.ScaleAttackMovePattern(Dir.LEFT, BoardMatrix.BOARD_SIZE),
            new MovePattern.ScaleAttackMovePattern(Dir.RIGHT, BoardMatrix.BOARD_SIZE)
    };

    private Rook(Alliance alliance, boolean hasMoved) {
        super(PieceType.ROOK, alliance, hasMoved);
    }

    @Override
    protected MovePattern[] getMovePatterns(Alliance alliance, Pos position) {
        return movePatterns;
    }

    @Override
    public Piece move() { return alliance == Alliance.WHITE ? WhiteMoved : BlackMoved; }

    public static final Rook White = new Rook(Alliance.WHITE, false);
    public static final Rook Black = new Rook(Alliance.BLACK, false);
    public static final Rook WhiteMoved = new Rook(Alliance.WHITE, true);
    public static final Rook BlackMoved = new Rook(Alliance.BLACK, true);
}
