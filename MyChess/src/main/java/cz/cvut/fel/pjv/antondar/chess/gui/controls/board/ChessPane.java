package cz.cvut.fel.pjv.antondar.chess.gui.controls.board;

import cz.cvut.fel.pjv.antondar.chess.gui.controls.ImageUtils;
import cz.cvut.fel.pjv.antondar.chess.gui.controls.StrategyContainer;
import cz.cvut.fel.pjv.antondar.chess.gui.controls.TileStrategy;
import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.board.Pos;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.BoardPiece;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.Piece;

import cz.cvut.fel.pjv.antondar.chess.gui.controls.PieceContainer;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Chess board control, extents classic javafx panel.
 */
public class ChessPane extends Pane {
    private Canvas canvas;
    private GraphicsContext gc;
    private ChessBoardGc boardConf;
    private Logger logger = Logger.getLogger(ChessPane.class.getName());

    /**
     * Move processing strategy.
     */
    @Setter
    public StrategyContainer tileStrategyContainer = new StrategyContainer(new TileStrategy.NullTileStrategy());

    /**
     * Board property for binding.
     */
    @Getter
    private final SimpleObjectProperty<BoardMatrix> boardProperty = new SimpleObjectProperty<BoardMatrix>(BoardMatrix.createEmpty());

    public ChessPane() {
        super();
        logger.log(Level.INFO, ()-> String.format("it's ChessPane width=" + this.getPrefWidth() + " height=" + this.getPrefHeight()));
//        System.out.println("it's ChessPane width=" + this.getPrefWidth() + " height=" + this.getPrefHeight());
        boardConf = new ChessBoardGc();
        canvas = new Canvas(boardConf.getWidth(), boardConf.getHeight());
        gc = canvas.getGraphicsContext2D();
        boardProperty.addListener(this::onBoardChanged);
        draw();
    }

    private void onBoardChanged(ObservableValue<? extends BoardMatrix> prop,
                                      BoardMatrix oldValue,
                                      BoardMatrix newValue) {
        redraw();
    }

    private void redraw(){
        getChildren().clear();
        draw();
    }

    /**
     * Draw control.
     */
    public void draw(){
        drawBoard();
        drawPiece();
    }

    private void drawBoard(){
        gc.clearRect(0,0, boardConf.getHeight(),this.getPrefHeight());
        final int ROWS_COUNT = 8;
        final int COLS_COUNT = 8;
        double cellSize = boardConf.getCollSize();
        final Color whiteColor = Color.valueOf("#EDEDED");
        final Color blackColor = Color.valueOf("#CDC5BF");

        boolean isWhiteTile = true;
        for(int i = 0; i < ROWS_COUNT; i++){
            isWhiteTile = !isWhiteTile;
            for(int j = 0; j < COLS_COUNT; j++){
                gc.setFill(isWhiteTile? whiteColor : blackColor);
                gc.fillRect(j*cellSize,i*cellSize, cellSize, cellSize);
                isWhiteTile = !isWhiteTile;
            }
        }

        gc.setStroke(Color.GRAY);
        gc.strokeRect(0, 0,cellSize*COLS_COUNT,cellSize*ROWS_COUNT);

        getChildren().add(canvas);
    }

    private final Map<PieceContainer, Integer> pieceContainer2Index = new HashMap<PieceContainer, Integer>();
    private final PieceContainer[] index2PieceContainer = new PieceContainer[BoardMatrix.BOARD_SIZE*BoardMatrix.BOARD_SIZE];

    private void drawPiece() {
        drawPieceContainers();
        BoardMatrix board = boardProperty.get();

        for (Pos pos : Pos.getAll()) {
            BoardPiece tile = board.getAt(pos);
            if (tile == BoardPiece.Empty)
                continue;

            Piece piece = tile.getPiece();
            Image image = ImageUtils.LoadImage(piece, this.getClass().getClassLoader());

            try {
                ImageView iv = index2PieceContainer[pos.getValue()].getImageView();
                iv.setImage(image);
            } catch (Exception e) {
                System.out.printf("Error initialize piece %d\n", pos);
                e.printStackTrace();
                throw e;
            }
        }
    }

    private void drawPieceContainers() {
        double collSize = boardConf.getCollSize();

        int cellsInLineCount = boardConf.getBoardSizeCells();
        for (int x = 0; x < cellsInLineCount; x++) {
            for (int y = 0; y < cellsInLineCount; y++) {
                int tileIndex = x + y * cellsInLineCount;

                PieceContainer sp = new PieceContainer(tileIndex, collSize, false, tileStrategyContainer);
                getChildren().add(sp);

                pieceContainer2Index.put(sp, tileIndex);
                index2PieceContainer[tileIndex] = sp;

                sp.setLayoutX(x*collSize);
                sp.setLayoutY(y*collSize);
            }
        }
    }

    /**
     * Graphic contexts, defines constants with properties of control
     */
    class ChessBoardGc {
        @Getter
        private final double height = 600;

        @Getter
        private final double startX = 0;

        @Getter
        private final double startY = 0;

        @Getter
        private final double width = 600;

        @Getter
        private final int boardSizeCells = 8;

        @Getter
        private final double collSize = width / boardSizeCells;
    }
}
