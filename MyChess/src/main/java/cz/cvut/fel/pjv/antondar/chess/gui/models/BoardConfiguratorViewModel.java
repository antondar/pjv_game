package cz.cvut.fel.pjv.antondar.chess.gui.models;

import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.board.Calculations;
import cz.cvut.fel.pjv.antondar.chess.gui.controls.StrategyContainer;
import cz.cvut.fel.pjv.antondar.chess.gui.controls.TileStrategy;
import javafx.beans.property.SimpleObjectProperty;
import lombok.Getter;

/**
 * View-model for board configuration window
 */
public class BoardConfiguratorViewModel {
    /**
     * Defines property for custom board, used in game config later.
     */
    @Getter
    private final SimpleObjectProperty<BoardMatrix> boardPattern = new SimpleObjectProperty<>(BoardMatrix.createEmpty());

    /**
     * Defines movement processing strategy for chess board pane.
     */
    @Getter
    private StrategyContainer boardStrategyContainer = new StrategyContainer(new TileStrategy.ConfigureBoardTileStrategy(boardPattern));

    /**
     * Defines movement processing strategy for piece stash.
     * Uses denied-strategy, because moving pieces into stash is not denied.
     */
    @Getter
    private StrategyContainer stashStrategyContainer = new StrategyContainer(new TileStrategy.DeniedTileStrategy());

    /**
     * Check if board valid at the end of custom board configuration.
     */
    public boolean checkBoardValid() {
        return Calculations.checkBoardValid(boardPattern.get());
    }
}
