package cz.cvut.fel.pjv.antondar.chess.engine.serialization;

import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.Game;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;

/**
 * Common methods for serialization.
 */
public class SerializationUtils {
    public static String alliance2Str(Alliance alliance) {
        return alliance.isWhite() ? "white" : "black";
    }

    public static String gameMode2Str(Game.Mode mode) {
        return mode == Game.Mode.PVE ? "pve" : "pvp";
    }
}
