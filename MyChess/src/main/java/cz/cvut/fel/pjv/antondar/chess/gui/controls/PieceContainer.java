package cz.cvut.fel.pjv.antondar.chess.gui.controls;

import cz.cvut.fel.pjv.antondar.chess.gui.controllers.GameController;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.Pane;
import lombok.Getter;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Container class for board piece image.
 * Contains drug'n'drop logic used for moves providing.
 */
public class PieceContainer extends Pane {
    /**
     * Index of tile occupied by piece, used for determining source or target of move
     */
    @Getter
    private final int tileIndex;

    /**
     * Container for piece image
     */
    @Getter
    private final ImageView imageView;

    /**
     * Defines if container used for populate piece (in stash)
     */
    @Getter
    private final boolean populate;

    private Logger logger = Logger.getLogger(PieceContainer.class.getName());

    // strategy for processing moves
    private StrategyContainer strategyContainer;

    public PieceContainer(int tileIndex, double collSize, boolean populate, StrategyContainer strategyContainer) {
        this.tileIndex = tileIndex;
        this.populate = populate;
        this.strategyContainer = strategyContainer;
        imageView = new ImageView();
        this.getChildren().add(imageView);

        prefWidth(collSize);
        minWidth(collSize);
        maxWidth(collSize);
        prefHeight(collSize);
        minHeight(collSize);
        maxHeight(collSize);

        imageView.setFitWidth(collSize);
        imageView.setFitHeight(collSize);

        subscribeToDnD(this);
    }

    // Subscribing to drug'n'drop
    private void subscribeToDnD(PieceContainer sp) {
        // Piece hooked for moving
        sp.setOnDragDetected(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                logger.log(Level.INFO, () -> String.format
                        ("Drag started Coords: %f, %f Scene coords: %f, %f Screen coords: %f %f\n", event.getX(), event.getY(),
                        event.getSceneX(), event.getSceneY(), event.getScreenX(), event.getScreenY()));
//                System.out.printf("Drag started Coords: %f, %f Scene coords: %f, %f Screen coords: %f %f\n", event.getX(), event.getY(),
//                        event.getSceneX(), event.getSceneY(), event.getScreenX(), event.getScreenY());

                Dragboard db = sp.startDragAndDrop(TransferMode.ANY);
                ClipboardContent content = new ClipboardContent();

                try {
                    ImageView iv = sp.getImageView();
                    content.putImage(iv.getImage());
                    iv.setImage(null);
                } catch (Exception e) {
                    logger.log(Level.SEVERE,"Error process DnD detected\n", e.getStackTrace());
//                    System.out.printf("Error process DnD detected\n");
//                    e.printStackTrace();
                }

                db.setContent(content);

                event.consume();
            }
        });

        // For moving from another containers
        sp.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                Object source = event.getGestureSource();

                if (source instanceof PieceContainer
                        && source != sp
                        && event.getDragboard().hasImage()
                        && strategyContainer.getStrategy().canMoveHere((PieceContainer)source, sp)) {

                    event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                }

                event.consume();
            }
        });

        // Processing state of drop container. Detecting if this place available for move or now.
        sp.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                logger.log(Level.INFO, ()-> String.format("Drag released Coords: %f, %f Scene coords: %f, %f Screen coords: %f %f\n", event.getX(), event.getY(),
                        event.getSceneX(), event.getSceneY(), event.getScreenX(), event.getScreenY()));
//                System.out.printf("Drag released Coords: %f, %f Scene coords: %f, %f Screen coords: %f %f\n", event.getX(), event.getY(),
//                        event.getSceneX(), event.getSceneY(), event.getScreenX(), event.getScreenY());

                Dragboard db = event.getDragboard();
                Object source = event.getGestureSource();
                boolean success = false;
                if (db.hasImage() && source instanceof PieceContainer) {
                    logger.log(Level.INFO, "Image found, putting to new location\n" );
//                    System.out.printf("Image found, putting to new location\n");

                    PieceContainer sourceContainer = (PieceContainer)source;

                    int sourceIndex = sourceContainer.tileIndex;
                    int destIndex = sp.tileIndex;
                    // Chck if move available
                    if (strategyContainer.getStrategy().canMoveHere(sourceContainer, sp)) {
                        Image image2 = db.getImage();

                        try {
                            ImageView iv = sp.getImageView();
                            iv.setImage(image2);
                            // Performing move
                            strategyContainer.getStrategy().performMove(sourceContainer, sp);
                            success = true;
                        } catch (Exception e) {
                            logger.log(Level.SEVERE, "Error process DnD dropped\n", e.getStackTrace());
//                            System.out.printf("Error process DnD dropped\n");
//                            e.printStackTrace();
                        }
                    } else {
                        logger.log(Level.INFO,()->String.format("Illegal move from %d to %d\n", sourceIndex, destIndex));
//                        System.out.printf("Illegal move from %d to %d\n", sourceIndex, destIndex);
                    }
                }

                event.setDropCompleted(success);

                event.consume();
            }
        });

        // Processing for invalid tile
        sp.setOnDragEntered(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                Object source = event.getGestureSource();
                if (source instanceof PieceContainer
                        && source != sp
                        && event.getDragboard().hasImage()) {

                    String style_inner = "-fx-border-color: red;"
                            + "-fx-border-width: 2;"
                            + "-fx-border-style: dotted;";

                    sp.setStyle(style_inner);
                }

                event.consume();
            }
        });

        // Exiting out of container
        sp.setOnDragExited(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                String style_inner = "-fx-border-width: 0;";
                sp.setStyle(style_inner);

                event.consume();
            }
        });

        // Consuming D'n'D operation
        sp.setOnDragDone(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                Object source = event.getGestureSource();
                Dragboard db = event.getDragboard();

                logger.log(Level.INFO,()->String.format
                        ("DnD done, mode: %s, source type: %s, has image: %b\n", event.getTransferMode(), source.getClass().getTypeName(), db.hasImage()));
//                System.out.printf("DnD done, mode: %s, source type: %s, has image: %b\n", event.getTransferMode(), source.getClass().getTypeName(), db.hasImage());

                if (event.getTransferMode() != TransferMode.MOVE
                        && source instanceof PieceContainer
                        && db.hasImage()) {

                    logger.log(Level.INFO, "DnD failed, restoring piece");
//                    System.out.printf("DnD failed, restoring piece");

                    PieceContainer sourceSp = (PieceContainer) source;

                    try {
                        ImageView sourceIv = sourceSp.getImageView();
                        sourceIv.setImage(db.getImage());
                    } catch (Exception e) {
                        logger.log(Level.SEVERE, "Error process DnD drag done\n", e.getStackTrace() );
//                        System.out.printf("Error process DnD drag done\n");
//                        e.printStackTrace();
                    }
                }
                event.consume();
            }
        });
    }
}
