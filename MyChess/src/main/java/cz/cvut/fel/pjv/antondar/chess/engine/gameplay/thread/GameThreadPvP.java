package cz.cvut.fel.pjv.antondar.chess.engine.gameplay.thread;

import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.Game;

/**
 * Represents game thread for PvP (Player vs Player)
 */
public class GameThreadPvP extends GameThread {
    /**
     * Creates game thread for PvP
     * @param game
     */
    public GameThreadPvP(Game game) {
        super(game);
    }

    @Override
    protected void iteration() { }
}
