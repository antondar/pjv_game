package cz.cvut.fel.pjv.antondar.chess.gui.controllers;

import cz.cvut.fel.pjv.antondar.App;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.stage.FileChooser;

import java.io.IOException;

/**
 * Controller for main window, managing FXML main window view
 */
public class MainWindowController {
    final FileChooser fileChooser = new FileChooser();

    /**
     * Handler for configure button click.
     */
    @FXML
    // setup new game
    private void setupGame() throws IOException {
        App.setRoot("GameSetup");
    }

    /**
     * Handler for load game button click. Not implemented, instead of raises alert.
     */
    @FXML
    // load game from pgn
    private void loadPgnViewer(ActionEvent ae) throws IOException {
        App.showModalAlert("Game load not implemented");
        //Node source = (Node) ae.getSource();
        //Window stage = source.getScene().getWindow();
        //File file = fileChooser.showOpenDialog(stage);
    }

}
