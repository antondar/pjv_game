package cz.cvut.fel.pjv.antondar.chess.engine.player;

import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.Move;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.BoardPiece;

import lombok.Getter;

import java.util.Collection;

/**
 * Represent player
 */
public abstract class Player {
    @Getter
    protected final Alliance alliance;

    protected final BoardMatrix board;

    @Getter
    protected final Collection<Move> legalMoves;

    @Getter
    protected final boolean isCheck;

    /**
     * Creates player with his alliance legal moves for his pieces depending on game progress
     * @param alliance player's color
     * @param board
     * @param legalMoves collection of moves for player's pieces
     * @param isCheck check if King is attacked
     */
    public Player(Alliance alliance, BoardMatrix board, Collection<Move> legalMoves, boolean isCheck) {
        this.alliance = alliance;
        this.board = board;
        this.legalMoves = legalMoves;
        this.isCheck = isCheck;
    }

    /**
     * Checkmate
     * @return boolean
     */
    public boolean isCheckMat() {
        return isCheck && legalMoves.isEmpty();
    }

    /**
     * Gets player's active pieces
     * @return collection of pieces
     */
    public Collection<BoardPiece> getActivePieces() {
        return board.getPieces(alliance);
    }
}
