package cz.cvut.fel.pjv.antondar.chess.engine.serialization;

import cz.cvut.fel.pjv.antondar.chess.engine.moves.Move;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.MoveHistory;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

/**
 * Defines class for move serializable type, acceptable by serializer.
 */
public class MoveCommandSerializableData {
    @Getter @Setter
    private final PositionSerializableData src;

    @Getter @Setter
    private final PositionSerializableData dest;

    public MoveCommandSerializableData(PositionSerializableData src, PositionSerializableData dest) {
        this.src = src;
        this.dest = dest;
    }

    /**
     * Constructs from engine logic move history class
     */
    public static MoveCommandSerializableData[] fromHistory(MoveHistory moveHistory) {
        Collection<Move> moves = moveHistory.getMoves();
        return moves
                .stream()
                .map(m -> fromMove(m))
                .toArray(MoveCommandSerializableData[]::new);
    }

    /**
     * Constructs from engine logic move class
     */
    public static MoveCommandSerializableData fromMove(Move move) {
        return new MoveCommandSerializableData(
                PositionSerializableData.fromPos(move.getSrc()),
                PositionSerializableData.fromPos(move.getDest()));
    }
}
