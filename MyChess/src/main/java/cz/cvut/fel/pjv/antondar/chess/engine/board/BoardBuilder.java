package cz.cvut.fel.pjv.antondar.chess.engine.board;

import cz.cvut.fel.pjv.antondar.chess.engine.moves.Move;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.BoardPiece;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.Piece;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;
import cz.cvut.fel.pjv.antondar.chess.engine.player.BlackPlayer;
import cz.cvut.fel.pjv.antondar.chess.engine.player.WhitePlayer;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Builds a new game board
 */
public class BoardBuilder {
    private Alliance nextMoveMaker;

    final BoardMatrix boardMatrix;

    public BoardBuilder() {
        this(BoardMatrix.createEmpty());
    }

    public BoardBuilder(final BoardMatrix boardMatrix) {
        this.boardMatrix = boardMatrix.clone();
    }

    /**
     * @param position of the piece
     * @param piece
     * @return BoardBuilder with a piece placed on the board
     */
    public BoardBuilder setPiece(final Pos position, final Piece piece) {
        this.boardMatrix.setAt(position, piece);
        return this;
    }

    /**
     * @param nextMoveMaker is player alliance
     * @return BoardBuilder with an active player
     */
    public BoardBuilder setMoveMaker(final Alliance nextMoveMaker) {
        this.nextMoveMaker = nextMoveMaker;
        return this;
    }

    /**
     * Builds board with current pieces, their moves and an active player.
     * @return Board
     */
    public Board build() {
        final Collection<BoardPiece> whitePieces = boardMatrix.getPieces(Alliance.WHITE);
        final Collection<BoardPiece> blackPieces = boardMatrix.getPieces(Alliance.BLACK);

        final Collection<Move> whiteMoves = Calculations.populateMoves(boardMatrix, whitePieces);
        final Collection<Move> blackMoves = Calculations.populateMoves(boardMatrix, blackPieces);

        final Collection<Move> whiteStandardLegalMoves = calculateLegalMoves(boardMatrix, whiteMoves);
        final Collection<Move> blackStandardLegalMoves = calculateLegalMoves(boardMatrix, blackMoves);

        final boolean isWhiteCheck = Calculations.isCheck(boardMatrix, Alliance.WHITE, blackMoves);
        final boolean isBlackCheck = Calculations.isCheck(boardMatrix, Alliance.BLACK, whiteMoves);
        
        final WhitePlayer whitePlayer = new WhitePlayer(boardMatrix, whiteStandardLegalMoves, isWhiteCheck);
        final BlackPlayer blackPlayer = new BlackPlayer(boardMatrix, blackStandardLegalMoves, isBlackCheck);

        return new Board(boardMatrix, whitePlayer, blackPlayer, nextMoveMaker);
    }

    private static Collection<Move> calculateLegalMoves(BoardMatrix board, Collection<Move> moves) {
        return moves
                .stream()
                .filter(move -> move.isLegal(board)) // use opponent moves here???
                .collect(Collectors.toList());
    }
}
