package cz.cvut.fel.pjv.antondar.chess.engine.gameplay;

import lombok.Getter;
import lombok.Setter;

/**
 * Represent meta information of game
 */
public class GameMeta {
    @Getter @Setter
    private final String annotator;

    @Getter @Setter
    private final String eventName;

    @Getter @Setter
    private final String gamePlace;

    @Getter @Setter
    private final String whitePlayerName;

    @Getter @Setter
    private final String blackPlayerName;

    /**
     * Creates meta information for game
     * @param annotator client
     * @param gameName game name
     * @param gamePlace venue of the game
     * @param whitePlayerName 1st player name
     * @param blackPlayerName 2nd player name
     */
    public GameMeta(String annotator, String gameName, String gamePlace, String whitePlayerName,
                    String blackPlayerName) {
        this.annotator = annotator;
        this.eventName = gameName;
        this.gamePlace = gamePlace;
        this.whitePlayerName = whitePlayerName;
        this.blackPlayerName = blackPlayerName;
    }

    /**
     * Instance GameMeta with no parameters
     */
    public static final GameMeta EMPTY = new GameMeta("", "", "", "", "");
}