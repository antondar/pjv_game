package cz.cvut.fel.pjv.antondar.chess.engine.pieces;

import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.board.Dir;
import cz.cvut.fel.pjv.antondar.chess.engine.board.Pos;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.Move;

import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;
import lombok.Getter;
import java.util.Collection;

/**
 * Represents piece on its position
 */
public class BoardPiece {
    @Getter
    private final Pos position;

    @Getter
    private final Piece piece;

    /**
     * Creates a board piece on the position
     * @param position  piece position
     * @param piece piece type
     */
    public BoardPiece(Pos position, Piece piece) {
        this.position = position;
        this.piece = piece;
    }

    /**
     * Collects moves for piece according to its position on a board
     * @param board
     * @return Collection of moves
     */
    public Collection<Move> populateMoves(BoardMatrix board) {
        return piece.populateMoves(board, position);
    }

    /**
     * Calculates destination position
     * @param dir is direction
     * @return destination position
     */
    public Pos calcDestination(Dir dir) {
        if (piece == null)
            throw new RuntimeException("Cannot calculate destination for empty piece");

        // Inverse move direction for black piece
        if (piece.alliance == Alliance.BLACK)
            dir = dir.inverse();

        // Recalculates position on board
        return position.offset(dir);
    }

    /**
     *  Empty tile
     */
    public static final BoardPiece Empty = new BoardPiece(Pos.OUT_OF_BOARD, null);
}
