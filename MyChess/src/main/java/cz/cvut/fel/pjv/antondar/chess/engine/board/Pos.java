package cz.cvut.fel.pjv.antondar.chess.engine.board;

import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * Represents positions of board's tiles
 */
public class Pos {
    public static final int SECOND_ROW = 1;
    public static final int SEVEN_ROW = 6;
    @Getter int value;

    protected Pos(int value) {
        this.value = value;
    }

    private Pos(int x, int y) {
        this(toValue(x, y));
    }

    public int getX() {
        return value % BoardMatrix.BOARD_SIZE;
    }

    public int getY() {
        return value / BoardMatrix.BOARD_SIZE;
    }

    /**
     * Calculates offset position
     * @param dir direction (x,y)
     * @return new position on a board
     */
    public Pos offset(Dir dir) {
        final int newX = getX() + dir.getX();
        final int newY = getY() + dir.getY();

        if (newX < 0 || newX >= BoardMatrix.BOARD_SIZE || newY < 0 || newY >= BoardMatrix.BOARD_SIZE)
            return OUT_OF_BOARD;

        // return square initials
        return cache[toValue(newX, newY)];
    }

    public boolean is2Row() {
        return getY() == SECOND_ROW;
    }

    public boolean is7Row() {
        return getY() == SEVEN_ROW;
    }

    /**
     * Check is tile's neighbor
     * @param other tile to check
     * @return boolean
     */
    public boolean isNearby(Pos other) {
        if (this == OUT_OF_BOARD || other == OUT_OF_BOARD)
            return false;

        return Math.abs(getX() - other.getX()) <= 1
                && Math.abs(getY() - other.getY()) <= 1;
    }

    @Override
    public String toString() {
        return String.format("%c%d", 'A' + getX(), getY() + 1 );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass())  return false;
        return value == ((Pos)o).value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    // Converts 2-dimensional value to 1-dimensional
    private static int toValue(final int x, final int y) {
        return y*BoardMatrix.BOARD_SIZE + x;
    }

    public static final Pos OUT_OF_BOARD = new Pos(-1, -1);

    // Sets square initials
    public static final Pos A1 = new Pos(0);
    public static final Pos B1 = new Pos(1);
    public static final Pos C1 = new Pos(2);
    public static final Pos D1 = new Pos(3);
    public static final Pos E1 = new Pos(4);
    public static final Pos F1 = new Pos(5);
    public static final Pos G1 = new Pos(6);
    public static final Pos H1 = new Pos(7);

    public static final Pos A2 = new Pos(8);
    public static final Pos B2 = new Pos(9);
    public static final Pos C2 = new Pos(10);
    public static final Pos D2 = new Pos(11);
    public static final Pos E2 = new Pos(12);
    public static final Pos F2 = new Pos(13);
    public static final Pos G2 = new Pos(14);
    public static final Pos H2 = new Pos(15);

    public static final Pos A3 = new Pos(16);
    public static final Pos B3 = new Pos(17);
    public static final Pos C3 = new Pos(18);
    public static final Pos D3 = new Pos(19);
    public static final Pos E3 = new Pos(20);
    public static final Pos F3 = new Pos(21);
    public static final Pos G3 = new Pos(22);
    public static final Pos H3 = new Pos(23);

    public static final Pos A4 = new Pos(24);
    public static final Pos B4 = new Pos(25);
    public static final Pos C4 = new Pos(26);
    public static final Pos D4 = new Pos(27);
    public static final Pos E4 = new Pos(28);
    public static final Pos F4 = new Pos(29);
    public static final Pos G4 = new Pos(30);
    public static final Pos H4 = new Pos(31);

    public static final Pos A5 = new Pos(32);
    public static final Pos B5 = new Pos(33);
    public static final Pos C5 = new Pos(34);
    public static final Pos D5 = new Pos(35);
    public static final Pos E5 = new Pos(36);
    public static final Pos F5 = new Pos(37);
    public static final Pos G5 = new Pos(38);
    public static final Pos H5 = new Pos(39);

    public static final Pos A6 = new Pos(40);
    public static final Pos B6 = new Pos(41);
    public static final Pos C6 = new Pos(42);
    public static final Pos D6 = new Pos(43);
    public static final Pos E6 = new Pos(44);
    public static final Pos F6 = new Pos(45);
    public static final Pos G6 = new Pos(46);
    public static final Pos H6 = new Pos(47);

    public static final Pos A7 = new Pos(48);
    public static final Pos B7 = new Pos(49);
    public static final Pos C7 = new Pos(50);
    public static final Pos D7 = new Pos(51);
    public static final Pos E7 = new Pos(52);
    public static final Pos F7 = new Pos(53);
    public static final Pos G7 = new Pos(54);
    public static final Pos H7 = new Pos(55);

    public static final Pos A8 = new Pos(56);
    public static final Pos B8 = new Pos(57);
    public static final Pos C8 = new Pos(58);
    public static final Pos D8 = new Pos(59);
    public static final Pos E8 = new Pos(60);
    public static final Pos F8 = new Pos(61);
    public static final Pos G8 = new Pos(62);
    public static final Pos H8 = new Pos(63);

    public static Iterable<Pos> getAll() {
        return Arrays.asList(cache);
    }

    /**
     * @param value tile index
     * @return cached position corresponding to tile index
     */
    public static Pos At(int value) {
        if (value < 0 || value >= 64)
            return OUT_OF_BOARD;

        return cache[value];
    }

    // cached positions
    private static final Pos[] cache = {
            A1, B1, C1, D1, E1, F1, G1, H1,
            A2, B2, C2, D2, E2, F2, G2, H2,
            A3, B3, C3, D3, E3, F3, G3, H3,
            A4, B4, C4, D4, E4, F4, G4, H4,
            A5, B5, C5, D5, E5, F5, G5, H5,
            A6, B6, C6, D6, E6, F6, G6, H6,
            A7, B7, C7, D7, E7, F7, G7, H7,
            A8, B8, C8, D8, E8, F8, G8, H8
    };
}
