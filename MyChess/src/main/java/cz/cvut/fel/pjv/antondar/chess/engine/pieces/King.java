package cz.cvut.fel.pjv.antondar.chess.engine.pieces;

import cz.cvut.fel.pjv.antondar.chess.engine.board.Dir;
import cz.cvut.fel.pjv.antondar.chess.engine.board.Pos;
import cz.cvut.fel.pjv.antondar.chess.engine.moves.MovePattern;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;
/**
 * Represents class of King
 */
public class King extends Piece {
    // Rules for move
    private static final MovePattern[] movePatterns = {
            // Jump moves
            new MovePattern.JumpMovePattern(Dir.UP),
            new MovePattern.JumpMovePattern(Dir.DOWN),
            new MovePattern.JumpMovePattern(Dir.LEFT),
            new MovePattern.JumpMovePattern(Dir.RIGHT),
            new MovePattern.JumpMovePattern(Dir.UP_LEFT),
            new MovePattern.JumpMovePattern(Dir.UP_RIGHT),
            new MovePattern.JumpMovePattern(Dir.DOWN_LEFT),
            new MovePattern.JumpMovePattern(Dir.DOWN_RIGHT),

            // Attack moves
            new MovePattern.AttackMovePattern(Dir.UP),
            new MovePattern.AttackMovePattern(Dir.DOWN),
            new MovePattern.AttackMovePattern(Dir.LEFT),
            new MovePattern.AttackMovePattern(Dir.RIGHT),
            new MovePattern.AttackMovePattern(Dir.UP_LEFT),
            new MovePattern.AttackMovePattern(Dir.UP_RIGHT),
            new MovePattern.AttackMovePattern(Dir.DOWN_LEFT),
            new MovePattern.AttackMovePattern(Dir.DOWN_RIGHT),

            // Castling moves
            new MovePattern.RightCastlingMovePattern(),
            new MovePattern.LeftCastlingMovePattern()
    };

    private King(Alliance alliance, boolean hasMoved) {
        super(PieceType.KING, alliance, hasMoved);
    }

    @Override
    protected MovePattern[] getMovePatterns(Alliance alliance, Pos position) {
        return movePatterns;
    }

    @Override
    public Piece move() { return alliance == Alliance.WHITE ? WhiteMoved : BlackMoved; }

    public static final King White = new King(Alliance.WHITE, false);
    public static final King Black = new King(Alliance.BLACK, false);
    public static final King WhiteMoved = new King(Alliance.WHITE, true);
    public static final King BlackMoved = new King(Alliance.BLACK, true);
}
