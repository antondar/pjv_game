package cz.cvut.fel.pjv.antondar.chess.engine.moves;

import cz.cvut.fel.pjv.antondar.chess.engine.board.Pos;
import lombok.Getter;

/**
 * Represent command to move from source to destination position
 */
public class MoveCommand {
    @Getter
    private Pos from;

    @Getter
    private Pos to;

    public MoveCommand(Pos from, Pos to) {
        this.from = from;
        this.to = to;
    }
}
