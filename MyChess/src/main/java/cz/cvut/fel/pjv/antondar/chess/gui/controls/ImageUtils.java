package cz.cvut.fel.pjv.antondar.chess.gui.controls;

import cz.cvut.fel.pjv.antondar.chess.engine.pieces.Piece;
import javafx.scene.image.Image;

import java.util.Locale;

/**
 * Util for load images
 */
public class ImageUtils {
    /**
     * Loads image from dir Images
     * @param piece
     * @param classLoader
     * @return image
     */
    public static Image LoadImage(Piece piece, ClassLoader classLoader) {
        String pieceName = piece.getPieceType().name();
        String pieceAllience = piece.getAlliance().name();
        String imgName = toFirstLetterUpperString(pieceName) + toFirstLetterUpperString(pieceAllience);
        String imgPath = "Images" + '/' + imgName + ".png";
        return new Image(classLoader.getResource(imgPath).toExternalForm());
    }

    private static String toFirstLetterUpperString(String str) {
        if (str == null || str.isBlank())
            return str;

        char[] symbols = str.toLowerCase(Locale.ROOT).toCharArray();
        symbols[0] = Character.toUpperCase(symbols[0]);

        return new String(symbols);
    }
}
