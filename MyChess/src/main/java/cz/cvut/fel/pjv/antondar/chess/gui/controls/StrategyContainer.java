package cz.cvut.fel.pjv.antondar.chess.gui.controls;

import lombok.Getter;
import lombok.Setter;

/**
 * Container for move processing strategy.
 * Used in chess pane and stash controls instead of real strategy to provide changing move strategy.
 */
public class StrategyContainer {
    @Getter @Setter
    private TileStrategy strategy;

    public StrategyContainer(TileStrategy strategy) {
        this.strategy = strategy;
    }
}
