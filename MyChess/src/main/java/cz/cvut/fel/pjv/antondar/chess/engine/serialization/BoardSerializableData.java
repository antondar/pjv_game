package cz.cvut.fel.pjv.antondar.chess.engine.serialization;

import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.pieces.BoardPiece;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

/**
 * Defines class for board serializable type, acceptable by serializer.
 */
public class BoardSerializableData {
    @Getter @Setter
    private final PieceSerializableData[] pieces;

    @Getter @Setter
    private final PositionSerializableData enpassantPos;

    public BoardSerializableData(PieceSerializableData[] pieces, PositionSerializableData enpassantPos) {
        this.pieces = pieces;
        this.enpassantPos = enpassantPos;
    }

    /**
     * Constructs from engine logic board matrix class
     */
    public static BoardSerializableData fromBoardMatrix(BoardMatrix board) {
        Collection<BoardPiece> bordPieces = board.getAllPieces();
        PieceSerializableData[] pieces = bordPieces
                .stream()
                .map(p -> PieceSerializableData.fromBoardPiece(p))
                .toArray(PieceSerializableData[]::new);

        PositionSerializableData enpassantPos = PositionSerializableData.fromPos(board.getEnPassant());

        return new BoardSerializableData(pieces, enpassantPos);
    }
}
