package cz.cvut.fel.pjv.antondar.chess.engine.gameplay;

import cz.cvut.fel.pjv.antondar.chess.engine.board.Board;
import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardBuilder;
import cz.cvut.fel.pjv.antondar.chess.engine.board.BoardMatrix;
import cz.cvut.fel.pjv.antondar.chess.engine.gameplay.thread.GameThreadFactory;
import cz.cvut.fel.pjv.antondar.chess.engine.player.Alliance;
import lombok.Getter;
import lombok.Setter;

import java.time.Duration;

/**
 * Factory entity for creating game.
 */
public class GameCreator {
    @Getter @Setter
    private Game.Mode mode;

    @Getter @Setter
    private BoardMatrix boardPattern;

    @Getter @Setter
    private Alliance startPlayer;

    @Getter @Setter
    private Alliance aiPlayer;

    @Getter @Setter
    private Duration timeLimit;

    /**
     * Initializing by defaults.
     */
    public GameCreator() {
        this.mode = Game.Mode.PVP;
        this.boardPattern = Board.createStandardBoardMatrix();
        this.startPlayer = Alliance.WHITE;
        this.aiPlayer = Alliance.BLACK;
        this.timeLimit = ChessClock.DEFAULT;
    }

    /**
     * Creates game
     * @return game
     */
    public Game create() {
        final GameThreadFactory gameThreadFactory = mode == Game.Mode.PVP
                ? GameThreadFactory.PvP
                : GameThreadFactory.PvE;

        final BoardBuilder builder = new BoardBuilder(boardPattern);
        builder.setMoveMaker(startPlayer);
        Board board = builder.build();

        return new Game(board, mode, startPlayer, aiPlayer, timeLimit, gameThreadFactory);
    }
}
