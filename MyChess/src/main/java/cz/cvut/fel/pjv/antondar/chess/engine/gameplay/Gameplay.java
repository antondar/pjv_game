package cz.cvut.fel.pjv.antondar.chess.engine.gameplay;

import lombok.Getter;
import lombok.Setter;
import lombok.Synchronized;

/**
 * Represent game play after configuration in Setup.
 * The class is used as a holder of game and provides some management functions.
 */
public class Gameplay {

    private Game game = null;

    private GameplayState state = GameplayState.Setup;

    // sets game config
    private GameConfig gameConfig = null;

    /**
     * Creates game by game configurations
     */
    public synchronized void createGame() {
        if (state == GameplayState.Play)
            throw new RuntimeException(String.format("Cannot play game now. Illegal state: %s", getState()));

        if (gameConfig == null)
            throw new RuntimeException(String.format("Game config is not configured"));

        final GameCreator creator = new GameCreator();
        creator.setMode(gameConfig.getGameMode());
        creator.setBoardPattern(gameConfig.getBoardPattern());
        creator.setStartPlayer(gameConfig.getStartPlayerAlliance());
        creator.setAiPlayer(gameConfig.getAiPlayerAlliance());
        creator.setTimeLimit(gameConfig.getTimeLimit());
        game = creator.create();
        state = GameplayState.Prepared;
    }

    /**
     * Start created game. Ignoring it, if game already started.
     */
    public synchronized void startGame() {
        // checks if game has not started yet
        if (state == GameplayState.Play)
            return;

        if (state != GameplayState.Prepared)
            throw new RuntimeException(String.format("Cannot play game now. Illegal state: %s", getState()));

        // starts the game thread if game was prepared
        game.start();
        state = GameplayState.Play;
    }

    /**
     * Method for termination game
     */
    public synchronized void terminateGame() {
        if(game != null) {
            //setups game result to unknown
            game.terminate(Game.GameResult.Unknown);
            game = null;
        }

        // return to setup state
        state = GameplayState.Setup;
    }

    /**
     * Method for resign game
     */
    public synchronized void resignGame() {
        // could be use only during play
        if (state != GameplayState.Play)
            throw new RuntimeException(String.format("Cannot play game now. Illegal state: %s", getState()));

        //setup game result to unknown
        game.terminate(Game.GameResult.Unknown);
        state = GameplayState.Setup;
    }

    public synchronized Game getGame() {
        return game;
    }

    public synchronized GameplayState getState() {
        return state;
    }

    public synchronized GameConfig getGameConfig() {
        return gameConfig;
    }

    public synchronized void setGameConfig(GameConfig gameConfig) {
        this.gameConfig = gameConfig;
    }

    /**
     * Possible game process states
     */
    public enum GameplayState {Setup, Prepared, Play }
}

