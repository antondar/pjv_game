module cz.cvut.fel.pjv.antondar {
    requires javafx.controls;
    requires javafx.graphics;
    requires java.logging;
    requires javafx.fxml;
    requires static lombok;
    requires com.google.gson;

    opens cz.cvut.fel.pjv.antondar to javafx.fxml;
    exports cz.cvut.fel.pjv.antondar;
    exports cz.cvut.fel.pjv.antondar.chess.gui;
    opens cz.cvut.fel.pjv.antondar.chess.gui to javafx.fxml;
    exports cz.cvut.fel.pjv.antondar.chess.engine.gameplay;
    opens cz.cvut.fel.pjv.antondar.chess.engine.gameplay to javafx.fxml, com.google.gson;
    exports cz.cvut.fel.pjv.antondar.chess.engine.moves;
    opens cz.cvut.fel.pjv.antondar.chess.engine.moves to javafx.fxml;
    exports cz.cvut.fel.pjv.antondar.chess.engine.gameplay.thread;
    opens cz.cvut.fel.pjv.antondar.chess.engine.gameplay.thread to javafx.fxml;
    exports cz.cvut.fel.pjv.antondar.chess.engine.serialization;
    opens cz.cvut.fel.pjv.antondar.chess.engine.serialization to javafx.fxml, com.google.gson;
    //exports cz.cvut.fel.pjv.antondar.chess.engine;
    //opens cz.cvut.fel.pjv.antondar.chess.engine to javafx.fxml;
    exports cz.cvut.fel.pjv.antondar.chess.gui.controllers;
    opens cz.cvut.fel.pjv.antondar.chess.gui.controllers to javafx.fxml;
    exports cz.cvut.fel.pjv.antondar.chess.gui.controls.board;
    opens cz.cvut.fel.pjv.antondar.chess.gui.controls.board to javafx.fxml;
    exports cz.cvut.fel.pjv.antondar.chess.gui.controls;
    opens cz.cvut.fel.pjv.antondar.chess.gui.controls to javafx.fxml;
    exports cz.cvut.fel.pjv.antondar.chess.gui.controls.stash;
    opens cz.cvut.fel.pjv.antondar.chess.gui.controls.stash to javafx.fxml;
}